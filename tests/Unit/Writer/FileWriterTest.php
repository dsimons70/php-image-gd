<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Writer;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Writer\FileWriter;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use SplFileObject;

/**
 * @covers \Goforit\ImageGD\Writer\FileWriter
 * @covers \Goforit\ImageGD\Writer\Writer
 * @covers \Goforit\ImageGD\ImageException
 */
class FileWriterTest extends TestCase
{
    private FileWriter $fileWriter;
    private Image $image;
    private PHPProphet $globalProphet;
    private vfsStreamDirectory $filesystem;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->fileWriter = FileWriter::create();
        $this->image = Image::forTrueColor(100, 100);
        $this->filesystem = vfsStream::setup('images');
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     * @dataProvider provideSupportedExtensions
     */
    public function testWrite(string $extension)
    {
        // Prepare
        $fileName = $this->getFileName($extension);

        // Execute
        $fileInfo = $this->fileWriter->write($this->image, $fileName);

        // Assert
        self::assertTrue($this->filesystem->hasChild($fileInfo->getFilename()));
    }

    /**
     * @test
     */
    public function testWriteWithNotWritableDirectoryWillThrowException()
    {
        // Prepare
        $this->filesystem->chmod(555);
        $fileName = $this->getFileName(Image::EXTENSION_PNG);

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        $this->fileWriter->write($this->image, $fileName);
    }

    /**
     * @test
     * @dataProvider provideSupportedExtensions
     */
    public function writeFileObject(string $extension)
    {
        // Prepare
        $fileObject = new SplFileObject($this->getFileName($extension), 'w+');

        // Execute
        $this->fileWriter->writeFileObject($this->image, $fileObject);

        // Assert
        self::assertTrue($this->filesystem->hasChild($fileObject->getFilename()));
    }

    /**
     * @test
     */
    public function testWriteFileObjectIsNotWritableWillThrowException()
    {
        // Prepare
        $filename = $this->getFileName(Image::EXTENSION_JPG);

        touch($filename);
        chmod($filename, 0444);

        $fileObject = new SplFileObject($filename, 'r');

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        $this->fileWriter->writeFileObject($this->image, $fileObject);
    }

    /**
     * @test
     */
    public function testWriteWithInvalidExtensionWillThrowAnException()
    {
        // Prepare
        $fileName = $this->getFileName('unsupported');

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        $this->fileWriter->write($this->image, $fileName);
    }

    private function getFileName(string $extension): string
    {
        $dimension = $this->image->getDimension();
        $fileName = $dimension->getWidth() . 'x' . $dimension->getHeight() . '.' . $extension;
        $pathName = $this->filesystem->url() . '/' . $fileName;

        return $pathName;
    }

    public function provideSupportedExtensions(): array
    {
        return [
            'test_jpg' => [Image::EXTENSION_JPG],
            'test_jpeg' => [Image::EXTENSION_JPEG],
            'test_gif' => [Image::EXTENSION_GIF],
            'test_png' => [Image::EXTENSION_PNG],
            'test_wbmp' => [Image::EXTENSION_WBMP],
            'test_xbm' => [Image::EXTENSION_XBM]
        ];
    }
}
