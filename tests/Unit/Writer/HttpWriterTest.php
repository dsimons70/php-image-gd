<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Writer;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Writer\HttpWriter;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Writer\HttpWriter
 * @covers \Goforit\ImageGD\Writer\Writer
 * @covers \Goforit\ImageGD\ImageException
 */
class HttpWriterTest extends TestCase
{
    private HttpWriter $httpWriter;
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Writer');
        $this->httpWriter = HttpWriter::create();
        $this->image = Image::forTrueColor(100, 100);
        assert_options(ASSERT_ACTIVE, false);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWritePng()
    {
        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagepng($this->image->getHandle(), null, 0)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writePng($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWritePngWithCompression()
    {
        // Prepare
        $this->httpWriter->setPngCompression(5);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagepng($this->image->getHandle(), null, 5)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writePng($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWritePngWithInvalidCompressionValue()
    {
        // Prepare
        $this->httpWriter->setPngCompression(2343);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagepng($this->image->getHandle(), null, 0)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writePng($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteJpg()
    {
        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagejpeg($this->image->getHandle(), null, 100)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeJpg($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteJpgWithQuality()
    {
        // Prepare
        $this->httpWriter->setJpgQuality(80);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagejpeg($this->image->getHandle(), null, 80)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeJpg($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteJpgWithInvalidQuality()
    {
        // Prepare
        $this->httpWriter->setJpgQuality(764545);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagejpeg($this->image->getHandle(), null, 100)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeJpg($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteGif()
    {
        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagegif($this->image->getHandle(), null)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeGif($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteXbm()
    {
        // Prepare
        $color = $this->image->allocateColor(ImageColor::black());

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagexbm($this->image->getHandle(), null, $color)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeXbm($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteXbmWithForegroundColor()
    {
        // Prepare
        $color = ImageColor::fromHex('#330033');
        $expectedColor = $this->image->allocateColor($color);
        $this->httpWriter->setXbmForegroundColor($color);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagexbm($this->image->getHandle(), null, $expectedColor)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeXbm($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteWbmp()
    {
        // Prepare
        $color = $this->image->allocateColor(ImageColor::black());

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagewbmp($this->image->getHandle(), null, $color)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeWbmp($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testWriteWbmpWithForegroundColor()
    {
        // Prepare
        $color = ImageColor::fromHex('#330033');
        $expectedColor = $this->image->allocateColor($color);
        $this->httpWriter->setWbmpForegroundColor($color);

        // Expect
        $this->globalProphecy->header(Argument::any())->shouldBeCalled();
        $this->globalProphecy->imagewbmp($this->image->getHandle(), null, $expectedColor)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $this->httpWriter->writeWbmp($this->image);
    }
}
