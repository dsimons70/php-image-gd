<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align;

use Goforit\ImageGD\Align\Alignment;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Alignment
 */
class AlignmentTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidAbsoluteAlignments
     */
    public function testValidAbsoluteAlignments($alignmentValue)
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedValue = $this->getExpectedValue($alignmentValue);
        $expectedType = Alignment::ABSOLUTE;

        // Execute
        $alignment = Alignment::fromString($alignmentValue);

        // Assert
        self::assertEquals($expectedValue, $alignment->toAbsolute());
        self::assertEquals(0, $alignment->toPercentage());
        self::assertTrue($alignment->isAbsolute());
        self::assertFalse($alignment->isPercentage());
        self::assertEquals($expectedValue . $expectedType, $alignment->__toString());
    }

    /**
     * @test
     * @dataProvider provideValidRelativeAlignments
     */
    public function testValidPercentageAlignments($alignmentValue)
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedValue = $this->getExpectedValue($alignmentValue);
        $expectedType = Alignment::PERCENTAGE;

        // Execute
        $alignment = Alignment::fromString($alignmentValue);

        // Assert
        self::assertEquals($expectedValue / 100, $alignment->toPercentage());
        self::assertEquals(0, $alignment->toAbsolute());
        self::assertFalse($alignment->isAbsolute());
        self::assertTrue($alignment->isPercentage());
        self::assertEquals($expectedValue . $expectedType, $alignment->__toString());
    }

    /**
     * @test
     * @dataProvider provideInvalidAlignments
     */
    public function testInvalidAlignmentsWillSetValidDefaultValues($alignmentValue)
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedValue = 0;
        $expectedType = Alignment::ABSOLUTE;

        // Execute
        $alignment = Alignment::fromString($alignmentValue);

        // Assert
        self::assertEquals($expectedValue, $alignment->toAbsolute());
        self::assertEquals(0, $alignment->toPercentage());
        self::assertTrue($alignment->isAbsolute());
        self::assertFalse($alignment->isPercentage());
        self::assertEquals($expectedValue . $expectedType, $alignment->__toString());
    }

    /**
     * @test
     */
    public function testPercentageValueNotInRangeWillSetDefaults()
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedValue = 0;
        $expectedType = Alignment::PERCENTAGE;

        // Execute
        $alignment = Alignment::fromString("110%");

        // Assert
        self::assertEquals($expectedValue, $alignment->toPercentage());
        self::assertEquals(0, $alignment->toAbsolute());
        self::assertFalse($alignment->isAbsolute());
        self::assertTrue($alignment->isPercentage());
        self::assertEquals($expectedValue . $expectedType, $alignment->__toString());
    }

    /**
     * @param mixed $alignment
     * @return int|null
     */
    private function getExpectedValue($alignment)
    {
        preg_match('/^\d+/', $alignment, $matches);

        if (count($matches) !== 1) {
            return null;
        }

        return intval($matches[0]);
    }

    /**
     * @return array
     */
    public function provideValidAbsoluteAlignments()
    {
        return [
            ['5px'],
            ['0px'],
            ['456px'],
            ['0056px'],
            ['45px'],
            ['0432px']
        ];
    }

    /**
     * @return array
     */
    public function provideValidRelativeAlignments()
    {
        return [
            ['100%'],
            ['0%'],
            ['56%'],
            ['99%'],
            ['099%'],
            ['34%']
        ];
    }

    /**
     * @return array
     */
    public function provideInvalidAlignments()
    {
        return [
            ['%'],
            ['0'],
            ['px'],
            ['-3px'],
            ['-50%'],
            ['34'],
            ['1a34px'],
            ['34PX'],
            ['1-50%']
        ];
    }
}
