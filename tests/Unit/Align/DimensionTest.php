<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align;

use Goforit\ImageGD\Align\Dimension;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Dimension
 */
class DimensionTest extends TestCase
{
    private Dimension $dimension;
    private int $expectedHeight = 100;
    private int $expectedWidth = 100;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        assert_options(ASSERT_ACTIVE, false);
        $this->dimension = Dimension::fromInt($this->expectedWidth, $this->expectedHeight);
    }

    /**
     * @test
     */
    public function testGetter()
    {
        // Assert
        self::assertEquals($this->expectedWidth, $this->dimension->getWidth());
        self::assertEquals($this->expectedHeight, $this->dimension->getHeight());
    }

    /**
     * @test
     */
    public function testGetterWithNegativeValuesWillConvertedToPositive()
    {
        // Prepare
        $expectedWidth = 160;
        $expectedHeight = 80;

        // Execute
        $dimension = Dimension::fromInt(-160, -80);

        // Assert
        self::assertEquals($expectedWidth, $dimension->getWidth());
        self::assertEquals($expectedHeight, $dimension->getHeight());
    }

    /**
     * @test
     */
    public function testGetRation()
    {
        // Prepare
        $expectedRatio = 1.0;

        // Execute
        $result = $this->dimension->getRatio();

        // Assert
        self::assertSame($expectedRatio, $result);
    }

    /**
     * @test
     */
    public function testResizeWidth()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(50, 50);

        // Execute
        $newDimension = $this->dimension->resizeWidth(50);

        // Assert
        self::assertEquals($expectedDimension, $newDimension);
    }

    /**
     * @test
     */
    public function testResizeHeight()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(90, 90);

        // Execute
        $newDimension = $this->dimension->resizeHeight(90);

        // Assert
        self::assertEquals($expectedDimension, $newDimension);
    }

    /**
     * @test
     */
    public function testResizePercental()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(66, 66);

        // Execute
        $newDimension = $this->dimension->resizePercental(66);

        // Assert
        self::assertEquals($expectedDimension, $newDimension);
    }

    /**
     * @test
     */
    public function testResizeIntoWillResizeWidth()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(30, 30);

        // Execute
        $newDimension = $this->dimension->resizeInto(Dimension::fromInt(30, 60));

        // Assert
        self::assertEquals($expectedDimension, $newDimension);
    }

    /**
     * @test
     */
    public function testResizeIntoWillResizeHeight()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(25, 25);

        // Execute
        $newDimension = $this->dimension->resizeInto(Dimension::fromInt(50, 25));

        // Assert
        self::assertEquals($expectedDimension, $newDimension);
    }
}
