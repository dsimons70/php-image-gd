<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\HorizontalAlignment;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\HorizontalAlignment
 */
class HorizontalAlignTest extends TestCase
{
    /**
     * @test
     */
    public function testLeft()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = HorizontalAlignment::left('10px');

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftX($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(10, $topLeftX);
    }

    /**
     * @test
     */
    public function testRight()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = HorizontalAlignment::right('10px');

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftX($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(80, $topLeftX);
    }

    /**
     * @test
     */
    public function testCenter()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = HorizontalAlignment::center();

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftX($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(45, $topLeftX);
    }
}
