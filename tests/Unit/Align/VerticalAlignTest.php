<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\VerticalAlignment;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\VerticalAlignment
 */
class VerticalAlignTest extends TestCase
{
    /**
     * @test
     */
    public function testTop()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = VerticalAlignment::top('10px');

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftY($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(10, $topLeftX);
    }

    /**
     * @test
     */
    public function testBottom()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = VerticalAlignment::bottom('10px');

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftY($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(80, $topLeftX);
    }

    /**
     * @test
     */
    public function testMiddle()
    {
        // Prepare
        $imageDimension = Dimension::fromInt(100, 100);
        $componentDimension = Dimension::fromInt(10, 10);
        $horizontalAlignment = VerticalAlignment::middle();

        // Execute
        $topLeftX = $horizontalAlignment->getTopLeftY($imageDimension, $componentDimension);

        // Assert
        self::assertEquals(45, $topLeftX);
    }
}
