<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align\Vertical;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Vertical\MiddleAlign;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Vertical\MiddleAlign
 */
class MiddleAlignTest extends TestCase
{
    /**
     * @test
     * @dataProvider getValidValuesDataProvider
     */
    public function testValidAlign(array $dimensionData)
    {
        // Prepare
        $imageDimension = $dimensionData['image_dimension'];
        $renderer_dimension = $dimensionData['renderer_dimension'];
        $expectedY = $dimensionData['expected_y'];

        // Execute
        $middleAlign = MiddleAlign::create();
        $processedY = $middleAlign->getTopLeftY($imageDimension, $renderer_dimension);

        // Assert
        self::assertEquals($expectedY, $processedY);
    }

    /**
     * @return array
     */
    public function getValidValuesDataProvider()
    {
        return [
            'test_245' => [
                [
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_y' => 245
                ]
            ],
            'test_0' => [
                [
                    'image_dimension' => Dimension::fromInt(50, 50),
                    'renderer_dimension' => Dimension::fromInt(50, 50),
                    'expected_y' => 0
                ]
            ],
            'test_prevent_negative' => [
                [
                    'image_dimension' => Dimension::fromInt(50, 50),
                    'renderer_dimension' => Dimension::fromInt(100, 100),
                    'expected_y' => 0
                ]
            ],
            'test_round_up_71' => [
                [
                    'image_dimension' => Dimension::fromInt(101, 151),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_y' => 71
                ],
            ],
            'test_round_up_both_19' => [
                [
                    'image_dimension' => Dimension::fromInt(101, 51),
                    'renderer_dimension' => Dimension::fromInt(11, 13),
                    'expected_y' => 19
                ]
            ]
        ];
    }
}
