<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align\Horizontal;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Horizontal\RightAlign;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Horizontal\RightAlign
 */
class RightAlignTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidValues
     */
    public function testValidAlign(array $dimensionData)
    {
        // Prepare
        $alignment = $dimensionData['alignment'];
        $imageDimension = $dimensionData['image_dimension'];
        $renderer_dimension = $dimensionData['renderer_dimension'];
        $expectedX = $dimensionData['expected_x'];

        if ($alignment === null) {
            $rightAlign = RightAlign::fromString();
        } else {
            $rightAlign = RightAlign::fromString($alignment);
        }

        // Execute
        $processedX = $rightAlign->getTopLeftX($imageDimension, $renderer_dimension);

        // Assert
        self::assertEquals($expectedX, $processedX);
    }

    /**
     * @return array
     */
    public function provideValidValues()
    {
        return [
            'test_null' => [
                [
                    'alignment' => null,
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 490
                ]
            ],
            'test_prevent_negative' => [
                [
                    'alignment' => '0px',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(510, 510),
                    'expected_x' => 0
                ]
            ],
            'test_0px' => [
                [
                    'alignment' => '0px',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 490
                ]
            ],
            'test_15px' => [
                [
                    'alignment' => '15px',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 475
                ]
            ],
            'test_0%' => [
                [
                    'alignment' => '0%',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 490
                ]
            ],
            'test_10%' => [
                [
                    'alignment' => '10%',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 440
                ]
            ],
            'test_11%' => [
                [
                    'alignment' => '11%',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 435
                ]
            ],
            'test_33%' => [
                [
                    'alignment' => '33%',
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 325
                ]
            ]

        ];
    }
}
