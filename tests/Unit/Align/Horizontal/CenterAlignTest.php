<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align\Horizontal;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Horizontal\CenterAlign;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Horizontal\CenterAlign
 */
class CenterAlignTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideValidValues
     */
    public function testValidAlign(array $dimensionData)
    {
        // Prepare
        $imageDimension = $dimensionData['image_dimension'];
        $renderer_dimension = $dimensionData['renderer_dimension'];
        $expectedX = $dimensionData['expected_x'];

        // Execute
        $centerAlign = CenterAlign::create();
        $processedX = $centerAlign->getTopLeftX($imageDimension, $renderer_dimension);

        // Assert
        self::assertEquals($expectedX, $processedX);
    }

    /**
     * @return array
     */
    public function provideValidValues()
    {
        return [
            'test_245' => [
                [
                    'image_dimension' => Dimension::fromInt(500, 500),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 245
                ]
            ],
            'test_0' => [
                [
                    'image_dimension' => Dimension::fromInt(50, 50),
                    'renderer_dimension' => Dimension::fromInt(50, 50),
                    'expected_x' => 0
                ]
            ],
            'test_prevent_negative' => [
                [
                    'image_dimension' => Dimension::fromInt(50, 50),
                    'renderer_dimension' => Dimension::fromInt(100, 100),
                    'expected_x' => 0
                ]
            ],
            'test_round_up_46' => [
                [
                    'image_dimension' => Dimension::fromInt(101, 50),
                    'renderer_dimension' => Dimension::fromInt(10, 10),
                    'expected_x' => 46
                ],
            ],
            'test_round_up_both_45' => [
                [
                    'image_dimension' => Dimension::fromInt(101, 50),
                    'renderer_dimension' => Dimension::fromInt(11, 10),
                    'expected_x' => 45
                ]
            ]
        ];
    }
}
