<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Align;

use Goforit\ImageGD\Align\Position;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Align\Position
 */
class PositionTest extends TestCase
{
    /**
     * @test
     */
    public function testValidValues()
    {
        // Prepare
        $expectedX = 100;
        $expectedY = 150;

        // Execute
        $position = Position::fromInt($expectedX, $expectedY);

        // Assert
        self::assertEquals($expectedX, $position->getX());
        self::assertEquals($expectedY, $position->getY());
    }

    /**
     * @test
     */
    public function testNegativeValuesWillBeConvertedToPositive()
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedX = 100;
        $expectedY = 150;

        // Execute
        $position = Position::fromInt(-100, -150);

        // Assert
        self::assertEquals($expectedX, $position->getX());
        self::assertEquals($expectedY, $position->getY());
    }
}
