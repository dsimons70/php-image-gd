<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests;

use Goforit\ImageGD\ImageColor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\ImageColor
 */
class ImageColorTest extends TestCase
{
    /**
     * @test
     */
    public function testInvalidAlphaWillSetDefaultValue()
    {
        assert_options(ASSERT_ACTIVE, false);
        $color = ImageColor::fromRgb(206, 200, 0, -23);
        self::assertFalse($color->hasAlphaTransparency());
        self::assertEquals(0, $color->getAlpha());
    }

    /**
     * @test
     */
    public function testInvalidColorWillSetDefaultValues()
    {
        assert_options(ASSERT_ACTIVE, false);
        $alpha = 127;
        $string = 'R' . 0 . 'G' . 0 . 'B' . 0 . 'A' . $alpha;
        $color = ImageColor::fromRgb(2564, 200, 0, $alpha);

        self::assertEquals(0, $color->getRed());
        self::assertEquals(0, $color->getGreen());
        self::assertEquals(0, $color->getBlue());
        self::assertEquals($alpha, $color->getAlpha());
        self::assertEquals($string, $color->__toString());
        self::assertTrue($color->hasAlphaTransparency());
    }

    /**
     * @test
     */
    public function testValidValuesWithAlpha()
    {
        $red   = 255;
        $green = 200;
        $blue  = 56;
        $alpha = 100;

        $string = 'R' . $red . 'G' . $green . 'B' . $blue . 'A' . $alpha;

        $color = ImageColor::fromRgb($red, $green, $blue, $alpha);

        self::assertEquals($red, $color->getRed());
        self::assertEquals($green, $color->getGreen());
        self::assertEquals($blue, $color->getBlue());
        self::assertEquals($alpha, $color->getAlpha());
        self::assertEquals($string, $color->__toString());
        self::assertTrue($color->hasAlphaTransparency());
    }

    /**
     * @test
     */
    public function testValidValuesWithoutAlpha()
    {
        $red   = 0;
        $green = 34;
        $blue  = 56;

        $string = 'R' . $red . 'G' . $green . 'B' . $blue . 'A';

        $color = ImageColor::fromRgb($red, $green, $blue);

        self::assertEquals($red, $color->getRed());
        self::assertEquals($green, $color->getGreen());
        self::assertEquals($blue, $color->getBlue());
        self::assertEquals(0, $color->getAlpha());
        self::assertEquals($string, $color->__toString());
        self::assertFalse($color->hasAlphaTransparency());
    }

    /**
     * @test
     * @dataProvider getColorsDataProvider
     * @param array $colors
     */
    public function testWithHexCodes(array $colors)
    {
        assert_options(ASSERT_ACTIVE, false);
        $actual = $colors['actual'];
        list($red, $green, $blue) = $colors['expected'];

        $string = 'R' . $red . 'G' . $green . 'B' . $blue . 'A';
        $color  = ImageColor::fromHex($actual);

        self::assertEquals($red, $color->getRed());
        self::assertEquals($green, $color->getGreen());
        self::assertEquals($blue, $color->getBlue());
        self::assertEquals(0, $color->getAlpha());
        self::assertEquals($string, $color->__toString());
        self::assertFalse($color->hasAlphaTransparency());
    }

    /**
     * @test
     */
    public function testImageColorWhite()
    {
        $color = ImageColor::white(42);

        self::assertEquals(255, $color->getRed());
        self::assertEquals(255, $color->getGreen());
        self::assertEquals(255, $color->getBlue());
        self::assertEquals(42, $color->getAlpha());
    }

    /**
     * @test
     */
    public function testImageColorBlack()
    {
        $color = ImageColor::black();

        self::assertEquals(0, $color->getRed());
        self::assertEquals(0, $color->getGreen());
        self::assertEquals(0, $color->getBlue());
        self::assertEquals(null, $color->getAlpha());
    }

    /**
     * @return array
     */
    public function getColorsDataProvider()
    {
        return [
            [
                [
                    "actual"   => '#00000',
                    "expected" => [0, 0, 0]
                ]
            ],
            [
                [
                    "actual"   => '#fff',
                    "expected" => [255, 255, 255]
                ]
            ],
            [
                [
                    "actual"   => '#ffA500',
                    "expected" => [255, 165, 0]
                ]
            ],
            [
                [
                    "actual"   => '#ee82ee',
                    "expected" => [238, 130, 238]
                ]
            ],
            [
                [
                    "actual"   => '#ba55d3',
                    "expected" => [186, 85, 211]
                ]
            ],
            [
                [
                    "actual"   => '#BA55D3',
                    "expected" => [186, 85, 211]
                ]
            ],
            [
                [
                    "actual"   => "#32cD32",
                    "expected" => [50, 205, 50]
                ]
            ],
            [
                [
                    "actual"   => "#ff00ff",
                    "expected" => [255, 0, 255]
                ]
            ],
            [
                [
                    "actual"   => "#ff00ffbroken",
                    "expected" => [0, 0, 0]
                ]
            ],
            [
                [
                    "actual"   => "ff00ff",
                    "expected" => [0, 0, 0]
                ]
            ],
            [
                [
                    "actual"   => "ff0",
                    "expected" => [0, 0, 0]
                ]
            ]
        ];
    }
}
