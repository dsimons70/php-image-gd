<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests;

use GdImage;
use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Renderer\Image\ImageRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Prophecy\ProphecyInterface;
use SplFileObject;
use TypeError;

/**
 * @covers \Goforit\ImageGD\Image
 * @covers \Goforit\ImageGD\ImageException
 */
class ImageTest extends TestCase
{
    use ProphecyTrait;

    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        assert_options(ASSERT_ACTIVE, false);
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD');
        $this->image = Image::forTrueColor(100, 100);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
        error_reporting(E_ALL);
    }

    /**
     * @test
     */
    public function testGetValidHandle(): void
    {
        // Assert
        self::assertInstanceOf(GdImage::class, $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testApplyRendererWillSetValidValues(): void
    {
        // Prepare
        $expected = Position::fromInt(10, 10);

        /** @var ObjectProphecy|ImageRenderer $imageRenderer */
        $imageRenderer = $this->prophesize(ImageRenderer::class);
        $imageRenderer->getDimension()->willReturn(Dimension::fromInt(10, 10));
        $imageRenderer->render($this->image, $expected)->shouldBeCalled();

        // Execute
        $this->image->applyRenderer(
            $imageRenderer->reveal(),
            HorizontalAlignment::left('10px'),
            VerticalAlignment::top('10px')
        );
    }

    /**
     * @test
     */
    public function testAllocateColor(): void
    {
        // Prepare
        $color = ImageColor::black();

        // Expect
        $this->globalProphecy->imagecolorallocatealpha(
            $this->image->getHandle(),
            $color->getRed(),
            $color->getGreen(),
            $color->getBlue()
        )->shouldNotBeCalled();

        $this->globalProphecy->reveal();

        // Assert
        self::assertEquals(0, $this->image->allocateColor($color));
    }

    /**
     * @test
     */
    public function testAllocateColorAlpha(): void
    {
        // Prepare
        $color = ImageColor::black(80);
        $expected = 1342177280;

        // Expect
        $this->globalProphecy->imagecolorallocatealpha(
            $this->image->getHandle(),
            $color->getRed(),
            $color->getGreen(),
            $color->getBlue(),
            $color->getAlpha()
        )->shouldBeCalled()->willReturn($expected);

        $this->globalProphecy->reveal();

        // Assert
        self::assertEquals($expected, $this->image->allocateColor($color));
    }

    /**
     * @test
     */
    public function testAllocateColorOnlyOnce(): void
    {
        // Prepare
        $color = ImageColor::black(90);
        $expected = 1;

        // Expect
        $this->globalProphecy->imagecolorallocatealpha(
            $this->image->getHandle(),
            $color->getRed(),
            $color->getGreen(),
            $color->getBlue(),
            $color->getAlpha()
        )->shouldBeCalledTimes(1)->willReturn($expected);

        $this->globalProphecy->reveal();

        // Assert
        self::assertEquals($expected, $this->image->allocateColor($color));
        self::assertEquals($expected, $this->image->allocateColor($color));
        self::assertEquals($expected, $this->image->allocateColor($color));
    }

    /**
     * @test
     */
    public function testGetDimension(): void
    {
        // Assert
        self::assertSame(100, $this->image->getDimension()->getHeight());
        self::assertSame(100, $this->image->getDimension()->getWidth());
    }

    /**
     * @test
     */
    public function testSetTransparency()
    {
        // Prepare
        $expectedTransparency = 80;

        // Execute
        $this->image->setTransparency($expectedTransparency);

        // Assert
        self::assertSame($expectedTransparency, $this->image->getTransparency());
    }

    /**
     * @test
     */
    public function testSetTransparencyWitInvalidValueWillBeRejected()
    {
        // Prepare
        $expectedTransparency = null;

        // Execute
        $this->image->setTransparency(23423);

        // Assert
        self::assertSame($expectedTransparency, $this->image->getTransparency());
    }

    /**
     * @test
     */
    public function testFromPaletteWithInvalidValuesWillThrowAnException(): void
    {
        // Prepare
        $expectedWidth = -345;
        $expectedHeight = 255;

        // Expect
        $this->expectException(TypeError::class);

        $this->globalProphecy->imagecreate($expectedWidth, $expectedHeight)->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        Image::forPalette($expectedWidth, $expectedHeight);
    }

    /**
     * @test
     */
    public function testFromPalette(): void
    {
        // Prepare
        $expectedWidth = 200;
        $expectedHeight = 255;

        // Execute
        $image = Image::forPalette($expectedWidth, $expectedHeight);

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertTrue($image->getHandle() instanceof GdImage);
    }

    /**
     * @test
     */
    public function testFromTrueColor(): void
    {
        // Prepare
        $expectedWidth = 200;
        $expectedHeight = 255;

        // Execute
        $image = Image::forTrueColor($expectedWidth, $expectedHeight);

        // Assert
        self::assertInstanceOf(GdImage::class, $image->getHandle());
        self::assertInstanceOf(Image::class, $image);
    }

    /**
     * @test
     */
    public function testFromFileWithNotExistentFileWillThrowException(): void
    {
        // imagecreate function create warning, suppress to test exception handling
        error_reporting(E_STRICT);

        // Prepare
        $expectedFile = '/does/not/exists.jpg';

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        Image::fromFile($expectedFile);
    }

    /**
     * @test
     */
    public function testFromFileWithNotSupportedExtensionWillThrowException(): void
    {
        // Prepare
        $expectedFile = __DIR__ . '/../Fixtures/fonts/BladeRunner.ttf';

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        Image::fromFile($expectedFile);
    }

    /**
     * @test
     */
    public function testFromFileWithNotProcessableImageFileWillThrowException(): void
    {
        // imagecreate function create warning, suppress to test exception handling
        error_reporting(E_STRICT);

        // Prepare
        $expectedFile = __DIR__ . '/../Fixtures/images/broken-image-file.jpg';

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        Image::fromFile($expectedFile);
    }

    /**
     * @test
     */
    public function testFromFileWithPng(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.png');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileWithGif(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.gif');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileWithJpeg(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.jpeg');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileWithJpg(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.jpg');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileWithWbmp(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.wbmp');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileWithXbm(): void
    {
        // Execute
        $image = Image::fromFile(__DIR__ . '/../Fixtures/images/f95-fcb.xbm');

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileObject(): void
    {
        // Execute
        $fileObject = new SplFileObject(__DIR__ . '/../Fixtures/images/f95-fcb.jpg');
        $image = Image::fromFileObject($fileObject);

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromFileObjectWithNotReadableFilesWillThrowException(): void
    {
        // Prepare
        /** @var SplFileObject|ObjectProphecy $fileObject */
        $fileObject = $this->prophesize(SplFileObject::class);

        // Expect
        $fileObject->isReadable()->willReturn(false);
        $fileObject->getPathname()->willReturn('not_readable_fake.jpg');
        $this->expectException(ImageException::class);

        // Execute
        Image::fromFileObject($fileObject->reveal());
    }

    /**
     * @test
     */
    public function testFromString(): void
    {
        // Prepare
        $file = __DIR__ . '/../Fixtures/images/f95-fcb.jpg';

        // Execute
        $image = Image::fromString(file_get_contents($file));

        // Assert
        self::assertTrue($image instanceof Image);
        self::assertInstanceOf(GdImage::class, $image->getHandle());
    }

    /**
     * @test
     */
    public function testFromStringWithInvalidDataWillThrowException(): void
    {
        // imagecreate function create warning, suppress to test exception handling
        error_reporting(E_STRICT);

        // Expect
        $this->expectException(TypeError::class);

        // Execute
        Image::fromString('hhfg');
    }

    /**
     * @test
     * @runInSeparateProcess
     */
    public function testDeletedImageObjectWillDestroyImageResourceTooFreeMemory()
    {
        // Prepare
        $file = __DIR__ . '/../Fixtures/images/f95-fcb.jpg';

        // Expect
        $image = Image::fromString(file_get_contents($file));

        $this->globalProphecy->imagedestroy($image->getHandle())->shouldBeCalled();
        $this->globalProphecy->reveal();

        // Execute
        $image = null;
        self::assertNull($image);
    }
}

