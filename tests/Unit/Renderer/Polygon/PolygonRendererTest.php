<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Polygon;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Polygon\Polygon;
use Goforit\ImageGD\Renderer\Polygon\PolygonRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\Polygon\PolygonRenderer
 */
class PolygonRendererTest extends TestCase
{
    private Image $targetImage;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\Polygon');
        $this->targetImage = Image::forTrueColor(100, 100);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     **/
    public function testRender()
    {
        // Prepare
        $polygon = new Polygon();
        $polygon->addPosition(Position::fromInt(24, 0));
        $polygon->addPosition(Position::fromInt(49, 49));
        $polygon->addPosition(Position::fromInt(0, 49));

        $expectedColor = ImageColor::black();
        $topLeftPosition = Position::fromInt(10, 10);
        $imagePolygon = PolygonRenderer::fromPolygon($polygon, $expectedColor, false);

        // Expect
        $this->globalProphecy->imagepolygon(
            $this->targetImage->getHandle(),
            $this->calculateExpectedVertices($polygon, $topLeftPosition),
            $this->targetImage->allocateColor($expectedColor)
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $imagePolygon->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals(50, $imagePolygon->getDimension()->getWidth());
        self::assertEquals(50, $imagePolygon->getDimension()->getHeight());
    }

    /**
     * @test
     */
    public function testRenderFilled()
    {
        // Prepare
        $polygon = new Polygon();
        $polygon->addPosition(Position::fromInt(0, 50));
        $polygon->addPosition(Position::fromInt(50, 0));
        $polygon->addPosition(Position::fromInt(99, 149));

        $expectedColor = ImageColor::black();
        $topLeftPosition = Position::fromInt(10, 10);
        $imagePolygon = PolygonRenderer::fromPolygon($polygon, $expectedColor, true);

        // Expect
        $this->globalProphecy->imagefilledpolygon(
            $this->targetImage->getHandle(),
            $this->calculateExpectedVertices($polygon, $topLeftPosition),
            $this->targetImage->allocateColor($expectedColor)
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $imagePolygon->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals(100, $imagePolygon->getDimension()->getWidth());
        self::assertEquals(150, $imagePolygon->getDimension()->getHeight());
    }

    /**
     * Transforms the polygon to the expected gd vertices array
     */
    private function calculateExpectedVertices(Polygon $polygon, Position $topLeftPosition): array
    {
        $vertices = [];

        /** @var Position $position */
        foreach ($polygon as $position) {
            $vertices[] = $position->getX() + $topLeftPosition->getX();
            $vertices[] = $position->getY() + $topLeftPosition->getY();
        }

        return $vertices;
    }
}
