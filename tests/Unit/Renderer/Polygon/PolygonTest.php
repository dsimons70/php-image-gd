<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Polygon;

use ArrayIterator;
use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Renderer\Polygon\Polygon;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Renderer\Polygon\Polygon
 * @covers \Goforit\ImageGD\ImageException
 */
class PolygonTest extends TestCase
{
    /**
     * @test
     */
    public function testWithValidValues()
    {
        // Prepare
        $polygon = new Polygon();
        $polygon->addPosition(Position::fromInt(0, 49));
        $polygon->addPosition(Position::fromInt(49, 0));
        $polygon->addPosition(Position::fromInt(99, 0));
        $polygon->addPosition(Position::fromInt(49, 99));

        // Assert
        self::assertCount(4, $polygon);
        self::assertEquals(Dimension::fromInt(100, 100), $polygon->getDimension());
        self::assertTrue($polygon->getIterator() instanceof ArrayIterator);
    }

    /**
     * @test
     */
    public function testToFewPositionsAddedWillThrowException()
    {
        // Prepare
        $polygon = new Polygon();
        $polygon->addPosition(Position::fromInt(49, 0));
        $polygon->addPosition(Position::fromInt(0, 49));

        $this->expectException(ImageException::class);

        // Assert
        self::assertCount(2, $polygon);
        self::assertEquals(Dimension::fromInt(0, 0), $polygon->getDimension());
    }
}
