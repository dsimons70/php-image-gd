<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Rectangle;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Renderer\Rectangle\Rectangle;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Renderer\Rectangle\Rectangle
 */
class RectangleTest extends TestCase
{
    /**
     * @test
     */
    public function testWithValidValues()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(45, 105);
        $expectedIsFilled = true;

        // Execute
        $rectangle = Rectangle::fromDimension($expectedDimension, $expectedIsFilled);

        // Assert
        self::assertEquals($expectedIsFilled, $rectangle->isFilled());
        self::assertEquals($expectedDimension, $rectangle->getDimension());
    }
}
