<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Rectangle;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Rectangle\Rectangle;
use Goforit\ImageGD\Renderer\Rectangle\RectangleRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\Rectangle\RectangleRenderer
 */
class RectangleRendererTest extends TestCase
{
    private Image $targetImage;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\Rectangle');
        $this->targetImage = Image::forTrueColor(100, 100);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testRenderRectangle()
    {
        // Prepare
        $topLeftPosition = Position::fromInt(10, 10);
        $rectangle = Rectangle::fromDimension(Dimension::fromInt(10, 10), false);
        $rectangleRenderer = RectangleRenderer::fromRectangle($rectangle, ImageColor::black());

        // Expect
        $this->globalProphecy->imagerectangle(
            $this->targetImage->getHandle(),
            10,
            10,
            19,
            19,
            $this->targetImage->allocateColor(ImageColor::black())
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $rectangleRenderer->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals(10, $rectangleRenderer->getDimension()->getWidth());
        self::assertEquals(10, $rectangleRenderer->getDimension()->getHeight());
    }

    /**
     * @test
     */
    public function testRenderFilledRectangle()
    {
        // Prepare
        $topLeftPosition = Position::fromInt(10, 10);
        $rectangle = Rectangle::fromDimension(Dimension::fromInt(10, 10), true);
        $rectangleRenderer = RectangleRenderer::fromRectangle($rectangle, ImageColor::black());

        // Expect
        $this->globalProphecy->imagefilledrectangle(
            $this->targetImage->getHandle(),
            10,
            10,
            19,
            19,
            $this->targetImage->allocateColor(ImageColor::black())
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $rectangleRenderer->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals(10, $rectangleRenderer->getDimension()->getWidth());
        self::assertEquals(10, $rectangleRenderer->getDimension()->getHeight());
    }
}
