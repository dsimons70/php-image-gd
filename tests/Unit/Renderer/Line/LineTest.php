<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Line;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Renderer\Line\Line;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Renderer\Line\Line
 */
class LineTest extends TestCase
{
    /**
     * @test
     */
    public function testForHorizontalAlign()
    {
        // Prepare
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(99, 0);
        $expectedThickness = 2;
        $expectedAngle = 0.0;

        // Execute
        $line = Line::forHorizontalAlign(100, $expectedThickness);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }

    /**
     * @test
     */
    public function testForVerticalAlign()
    {
        // Prepare
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(0, 199);
        $expectedThickness = 6;
        $expectedAngle = 90.0;

        // Execute
        $line = Line::forVerticalAlign(200, $expectedThickness);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }

    /**
     * @test
     */
    public function testFromPositions()
    {
        // Prepare
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(100, 100);
        $expectedThickness = 6;
        $expectedAngle = 45.0;

        // Execute
        $line = Line::fromPositions($expectedStartPosition, $expectedEndPosition, $expectedThickness);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }

    /**
     * @test
     */
    public function testInvalidThicknessWillUseDefaultValue()
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(0, 19);
        $expectedThickness = 1;
        $expectedAngle = 90.0;

        // Execute
        $line = Line::forVerticalAlign(20, 0);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }

    /**
     * @test
     */
    public function testInvalidHeightWillUseDefaultValue()
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(0, 99);
        $expectedThickness = 2;
        $expectedAngle = 90.0;

        // Execute
        $line = Line::forVerticalAlign(-5, 2);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }

    /**
     * @test
     */
    public function testInvalidWidthWillUseDefaultValue()
    {
        // Prepare
        assert_options(ASSERT_ACTIVE, false);
        $expectedStartPosition = Position::fromInt(0, 0);
        $expectedEndPosition = Position::fromInt(99, 0);
        $expectedThickness = 2;
        $expectedAngle = 0.0;

        // Execute
        $line = Line::forHorizontalAlign(-5, 2);

        // Assert
        self::assertEquals($expectedStartPosition, $line->getStartPosition());
        self::assertEquals($expectedEndPosition, $line->getEndPosition());
        self::assertSame($expectedThickness, $line->getThickness());
        self::assertSame($expectedAngle, $line->getAngle());
    }
}
