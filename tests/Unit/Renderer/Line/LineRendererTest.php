<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Line;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Line\Line;
use Goforit\ImageGD\Renderer\Line\LineRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\Line\LineRenderer
 */
class LineRendererTest extends TestCase
{
    private Image $targetImage;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\Line');
        $this->targetImage = Image::forTrueColor(100, 100);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testRenderHorizontal()
    {
        // Prepare
        $line = Line::forHorizontalAlign(50, 2);
        $lineRenderer = LineRenderer::fromLine($line, ImageColor::black());
        $topLeftPosition = Position::fromInt(10, 10);
        $expectedDimension = Dimension::fromInt(50, 2);

        // Expect
        $this->globalProphecy->imagesetthickness(
            $this->targetImage->getHandle(),
            2
        )->shouldBeCalled();

        $this->globalProphecy->imageline(
            $this->targetImage->getHandle(),
            $topLeftPosition->getX(),
            11,
            59,
            11,
            $this->targetImage->allocateColor(ImageColor::black())
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $lineRenderer->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals($expectedDimension, $lineRenderer->getDimension());
    }

    /**
     * @test
     */
    public function testRenderVerticalLine()
    {
        // Prepare
        $line = Line::forVerticalAlign(50, 1);
        $lineRenderer = LineRenderer::fromLine($line, ImageColor::black());
        $topLeftPosition = Position::fromInt(10, 10);
        $expectedDimension = Dimension::fromInt(1, 50);

        // Expect
        $this->globalProphecy->imagesetthickness(
            $this->targetImage->getHandle(),
            1
        )->shouldBeCalled();

        $this->globalProphecy->imageline(
            $this->targetImage->getHandle(),
            10,
            10,
            10,
            59,
            $this->targetImage->allocateColor(ImageColor::black())
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $lineRenderer->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals($expectedDimension, $lineRenderer->getDimension());
    }

    /**
     * @test
     */
    public function testRenderFreeLine()
    {
        // Prepare
        $startPosition = Position::fromInt(0, 0);
        $endPosition = Position::fromInt(100, 100);
        $topLeftPosition = Position::fromInt(10, 10);
        $expectedDimension = Dimension::fromInt(100, 100);
        $line = Line::fromPositions($startPosition, $endPosition);
        $lineRenderer = LineRenderer::fromLine($line, ImageColor::black());

        // Expect
        $this->globalProphecy->imagesetthickness(
            $this->targetImage->getHandle(),
            1
        )->shouldBeCalled();

        $this->globalProphecy->imageline(
            $this->targetImage->getHandle(),
            10,
            10,
            110,
            110,
            $this->targetImage->allocateColor(ImageColor::black())
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $lineRenderer->render($this->targetImage, $topLeftPosition);

        // Assert
        self::assertEquals($expectedDimension, $lineRenderer->getDimension());
    }
}
