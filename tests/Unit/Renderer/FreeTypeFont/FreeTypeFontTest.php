<?php
/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\FreeTypeFont;

use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFont;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFont
 */
class FreeTypeFontTest extends TestCase
{
    /**
     * @test
     */
    public function testWithValidValues(): void
    {
        // Prepare
        $expectedSize = 5;
        $expectedFontFile = dirname(__DIR__, 3) . '/Fixtures/fonts/BladeRunner.ttf';
        $expectedAngle = 90;

        // Execute
        $freeTypeFont = FreeTypeFont::fromFontFile($expectedFontFile, $expectedSize, $expectedAngle);

        // Assert
        self::assertSame($expectedFontFile, $freeTypeFont->getFontFile());
        self::assertSame($expectedSize, $freeTypeFont->getSize());
        self::assertSame($expectedAngle, $freeTypeFont->getAngle());
    }

    /**
     * @test
     */
    public function testInvalidFontFileWillThrowException(): void
    {
        // Prepare
        $expectedSize = 5;
        $expectedFontFile = dirname(__DIR__, 3) . '/Fixtures/fonts/NotExist.ttf';
        $expectedAngle = 90;

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        FreeTypeFont::fromFontFile($expectedFontFile, $expectedSize, $expectedAngle);
    }

    /**
     * @test
     */
    public function testInvalidFileExtensionWillThrowException(): void
    {
        // Prepare
        $expectedSize = 5;
        $expectedFontFile = dirname(__DIR__, 3) . '/Fixtures/images/bladerunner.jpg';
        $expectedAngle = 90;

        // Expect
        $this->expectException(ImageException::class);

        // Execute
        FreeTypeFont::fromFontFile($expectedFontFile, $expectedSize, $expectedAngle);
    }
}
