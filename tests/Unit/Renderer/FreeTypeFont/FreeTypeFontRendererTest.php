<?php

/*
 * This file is part of the Goforit\Image\GD\Tests package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\FreeTypeFont;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFont;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFontRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFontRenderer
 */
class FreeTypeFontRendererTest extends TestCase
{
    private Image $targetImage;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\FreeTypeFont');
        $this->targetImage = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testRender()
    {
        // Prepare
        $expectedText = 'Prophecy rocks';
        $expectedColor = ImageColor::black();

        $expectedFont = FreeTypeFont::fromFontFile(
            dirname(__DIR__, 3) . '/Fixtures/fonts/BladeRunner.ttf',
            16,
            0
        );

        $topLeftPosition = Position::fromInt(10, 10);
        $freeTypeText = FreeTypeFontRenderer::fromFont($expectedFont, $expectedColor, $expectedText);

        // Expect
        $this->globalProphecy->imagefttext(
            $this->targetImage->getHandle(),
            $expectedFont->getSize(),
            $expectedFont->getAngle(),
            10,
            25,
            $this->targetImage->allocateColor($expectedColor),
            $expectedFont->getFontFile(),
            $expectedText
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $freeTypeText->render($this->targetImage, $topLeftPosition);
    }
}
