<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Arc;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Renderer\Arc\Arc;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Renderer\Arc\Arc
 */
class ArcTest extends TestCase
{
    /**
     * @test
     */
    public function testForCircleOrEllipse()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(200, 200);
        $expectedStartDegree = 0;
        $expectedEndDegree = 360;
        $expectedThickness = 5;
        $expectedFillStyle = IMG_ARC_NOFILL;
        $expectedCenterPosition = Position::fromInt(100, 100);

        // Execute
        $arc = Arc::forCircleOrEllipse($expectedDimension, $expectedThickness);

        // Assert
        self::assertSame($expectedDimension, $arc->getDimension());
        self::assertSame($expectedStartDegree, $arc->getStartDegree());
        self::assertSame($expectedEndDegree, $arc->getEndDegree());
        self::assertSame($expectedThickness, $arc->getThickness());
        self::assertSame($expectedFillStyle, $arc->getFillStyle());
        self::assertEquals($expectedCenterPosition, $arc->getCenterPosition());
        self::assertFalse($arc->isFilled());
    }

    /**
     * @test
     */
    public function testForFilledCircleOrEllipse()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(201, 101);
        $expectedStartDegree = 0;
        $expectedEndDegree = 360;
        $expectedThickness = 0;
        $expectedFillStyle = IMG_ARC_ROUNDED;
        $expectedCenterPosition = Position::fromInt(101, 51);

        // Execute
        $arc = Arc::forFilledCircleOrEllipse($expectedDimension, $expectedThickness);

        // Assert
        self::assertSame($expectedDimension, $arc->getDimension());
        self::assertSame($expectedStartDegree, $arc->getStartDegree());
        self::assertSame($expectedEndDegree, $arc->getEndDegree());
        self::assertSame($expectedThickness, $arc->getThickness());
        self::assertSame($expectedFillStyle, $arc->getFillStyle());
        self::assertEquals($expectedCenterPosition, $arc->getCenterPosition());
        self::assertTrue($arc->isFilled());
    }

    /**
     * @test
     */
    public function testForArc()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(202, 102);
        $expectedStartDegree = 90;
        $expectedEndDegree = 180;
        $expectedThickness = 1;
        $expectedFillStyle = IMG_ARC_NOFILL;
        $expectedCenterPosition = Position::fromInt(101, 51);

        // Execute
        $arc = Arc::forArc($expectedDimension, $expectedStartDegree, 90, $expectedThickness);

        // Assert
        self::assertSame($expectedDimension, $arc->getDimension());
        self::assertSame($expectedStartDegree, $arc->getStartDegree());
        self::assertSame($expectedEndDegree, $arc->getEndDegree());
        self::assertSame($expectedThickness, $arc->getThickness());
        self::assertSame($expectedFillStyle, $arc->getFillStyle());
        self::assertEquals($expectedCenterPosition, $arc->getCenterPosition());
        self::assertFalse($arc->isFilled());
    }

    /**
     * @test
     */
    public function testForOutlinedPie()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(25, 50);
        $expectedStartDegree = 0;
        $expectedEndDegree = 45;
        $expectedThickness = 2;
        $expectedFillStyle = IMG_ARC_NOFILL + IMG_ARC_EDGED;
        $expectedCenterPosition = Position::fromInt(13, 25);

        // Execute
        $arc = Arc::forPie($expectedDimension, $expectedStartDegree, 45, $expectedThickness);

        // Assert
        self::assertSame($expectedDimension, $arc->getDimension());
        self::assertSame($expectedStartDegree, $arc->getStartDegree());
        self::assertSame($expectedEndDegree, $arc->getEndDegree());
        self::assertSame($expectedThickness, $arc->getThickness());
        self::assertSame($expectedFillStyle, $arc->getFillStyle());
        self::assertEquals($expectedCenterPosition, $arc->getCenterPosition());
        self::assertFalse($arc->isFilled());
    }

    /**
     * @test
     */
    public function testForFilledPie()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(500, 600);
        $expectedStartDegree = 180;
        $expectedEndDegree = 225;
        $expectedThickness = 0;
        $expectedFillStyle = IMG_ARC_PIE;
        $expectedCenterPosition = Position::fromInt(250, 300);

        // Execute
        $arc = Arc::forFilledPie($expectedDimension, $expectedStartDegree, 45);

        // Assert
        self::assertSame($expectedDimension, $arc->getDimension());
        self::assertSame($expectedStartDegree, $arc->getStartDegree());
        self::assertSame($expectedEndDegree, $arc->getEndDegree());
        self::assertSame($expectedThickness, $arc->getThickness());
        self::assertSame($expectedFillStyle, $arc->getFillStyle());
        self::assertEquals($expectedCenterPosition, $arc->getCenterPosition());
        self::assertTrue($arc->isFilled());
    }
}
