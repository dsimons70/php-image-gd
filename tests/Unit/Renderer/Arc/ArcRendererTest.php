<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Arc;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Arc\Arc;
use Goforit\ImageGD\Renderer\Arc\ArcRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\Arc\ArcRenderer
 */
class ArcRendererTest extends TestCase
{
    private Image $targetImage;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\Arc');
        $this->targetImage = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testRender()
    {
        // Prepare
        $expectedWidth = 150;
        $expectedHeight = 100;
        $dimension = Dimension::fromInt($expectedWidth, $expectedHeight);
        $expectedThickness = 2;
        $expectedColor = ImageColor::black();
        $expectedArc = Arc::forCircleOrEllipse($dimension, $expectedThickness);
        $renderer = ArcRenderer::fromArc($expectedArc, $expectedColor);

        // Expect
        $this->globalProphecy->imagesetthickness(
            $this->targetImage->getHandle(),
            $expectedThickness
        )->shouldBeCalled();

        $this->globalProphecy->imagefilledarc(
            $this->targetImage->getHandle(),
            75,
            50,
            $expectedWidth - $expectedThickness,
            $expectedHeight - $expectedThickness,
            0,
            360,
            $this->targetImage->allocateColor($expectedColor),
            $expectedArc->getFillStyle()
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $renderer->render($this->targetImage, Position::fromInt(0, 0));

        // Assert
        self::assertSame($expectedWidth, $renderer->getDimension()->getWidth());
        self::assertSame($expectedHeight, $renderer->getDimension()->getHeight());
    }

    /**
     * @test
     */
    public function testRenderWithoutImageSetThickness()
    {
        // Prepare
        $expectedWidth = 150;
        $expectedHeight = 100;
        $expectedColor = ImageColor::black();
        $expectedArc = Arc::forFilledCircleOrEllipse(Dimension::fromInt($expectedWidth, $expectedHeight));
        $renderer = ArcRenderer::fromArc($expectedArc, $expectedColor);

        // Expect
        $this->globalProphecy->imagesetthickness(Argument::cetera())->shouldNotBeCalled();

        $this->globalProphecy->imagefilledarc(
            $this->targetImage->getHandle(),
            75,
            50,
            $expectedWidth,
            $expectedHeight,
            0,
            360,
            $this->targetImage->allocateColor($expectedColor),
            $expectedArc->getFillStyle()
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $renderer->render($this->targetImage, Position::fromInt(0, 0));

        // Assert
        self::assertSame($expectedWidth, $renderer->getDimension()->getWidth());
        self::assertSame($expectedHeight, $renderer->getDimension()->getHeight());
    }
}
