<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer\Image;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Renderer\Image\ImageRenderer;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Renderer\Image\ImageRenderer
 */
class ImageRendererTest extends TestCase
{
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;
    private ImageRenderer $imageRenderer;
    private Image $image;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Renderer\Image');
        $this->image = Image::forTrueColor(100, 100);
        $this->imageRenderer = ImageRenderer::fromImage($this->image);
        assert_options(ASSERT_ACTIVE, false);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testInvalidTransparencyValueWillBeRejected()
    {
        // Prepare
        $targetImage = Image::forPalette(200, 200);
        $topLeftPosition = Position::fromInt(11, 11);

        // Expect
        $this->globalProphecy->imagecopyresampled(Argument::cetera())->shouldBeCalled();

        $this->globalProphecy->reveal();
        $this->image->setTransparency(1312);
        $this->imageRenderer->render($targetImage, $topLeftPosition);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testRenderWithoutTransparency()
    {
        // Prepare
        $targetImage = Image::forPalette(200, 200);
        $topLeftPosition = Position::fromInt(11, 11);

        // Expect
        $this->globalProphecy->imagecopyresampled(
            $targetImage->getHandle(),
            $this->image->getHandle(),
            $topLeftPosition->getX(),
            $topLeftPosition->getY(),
            0,
            0,
            $this->imageRenderer->getDimension()->getWidth(),
            $this->imageRenderer->getDimension()->getHeight(),
            $this->imageRenderer->getDimension()->getWidth(),
            $this->imageRenderer->getDimension()->getHeight()
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $this->imageRenderer->render($targetImage, $topLeftPosition);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testRenderWithTransparency()
    {
        // Prepare
        $targetImage = Image::forPalette(200, 200);
        $topLeftPosition = Position::fromInt(11, 11);
        $transparency = 80;

        $this->image->setTransparency($transparency);

        // Expect
        $this->globalProphecy->imagecopymerge(
            $targetImage->getHandle(),
            $this->image->getHandle(),
            $topLeftPosition->getX(),
            $topLeftPosition->getY(),
            0,
            0,
            $this->imageRenderer->getDimension()->getWidth(),
            $this->imageRenderer->getDimension()->getHeight(),
            $transparency
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $this->imageRenderer->render($targetImage, $topLeftPosition);
    }
}
