<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Renderer;

use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Renderer\Renderer;
use Goforit\ImageGD\Renderer\RendererChain;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Renderer\RendererChain
 */
class RendererChainTest extends TestCase
{
    use ProphecyTrait;

    private RendererChain $rendererChain;
    private Renderer|ObjectProphecy $firstRenderer;
    private Renderer|ObjectProphecy $secondRenderer;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->firstRenderer = $this->prophesize(Renderer::class);
        $this->secondRenderer = $this->prophesize(Renderer::class);
        $this->rendererChain = new RendererChain();
    }

    /**
     * @test
     */
    public function testAddRendererWillThrowExceptionIfRendererAlreadyRegistered()
    {
        // Prepare
        $horizontalAlign = HorizontalAlignment::center();
        $verticalAlign = VerticalAlignment::middle();

        //Expect
        $this->expectException(ImageException::class);

        // Execute
        $this->rendererChain->addRenderer($this->firstRenderer->reveal(), $horizontalAlign, $verticalAlign);
        $this->rendererChain->addRenderer($this->firstRenderer->reveal(), $horizontalAlign, $verticalAlign);
    }

    /**
     * @test
     */
    public function testProcess()
    {
        // Prepare
        $horizontalAlign = HorizontalAlignment::center();
        $verticalAlign = VerticalAlignment::middle();

        /** @var Image|ObjectProphecy $expectedImage */
        $expectedImage = $this->prophesize(Image::class);

        $this->rendererChain->addRenderer($this->firstRenderer->reveal(), $horizontalAlign, $verticalAlign);
        $this->rendererChain->addRenderer($this->secondRenderer->reveal(), $horizontalAlign, $verticalAlign);

        //Expect
        $expectedImage->applyRenderer($this->firstRenderer, $horizontalAlign, $verticalAlign)->shouldBeCalled();
        $expectedImage->applyRenderer($this->secondRenderer, $horizontalAlign, $verticalAlign)->shouldBeCalled();

        // Execute
        $image = $this->rendererChain->render($expectedImage->reveal());

        // Assert
        self::assertSame($expectedImage->reveal(), $image);
    }
}
