<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Processor\FilterProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\FilterProcessor
 * @covers \Goforit\ImageGD\ImageException
 */
class FilterProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testUnsuccessfulFilterCallWillThrowImageException()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forSelectiveBlur();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_SELECTIVE_BLUR
        )->shouldBeCalled()->willReturn(false);

        $this->globalProphecy->reveal();
        $this->expectException(ImageException::class);

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForSelectiveBlur()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forSelectiveBlur();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_SELECTIVE_BLUR
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForGaussianBlur()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forGaussianBlur();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_GAUSSIAN_BLUR
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForSmoothing()
    {
        // Prepare
        $expectedLevel = 8;
        $filterProcessor = FilterProcessor::forSmoothing($expectedLevel);

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_SMOOTH,
            $expectedLevel
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForColorReverse()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forColorReverse();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_NEGATE
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForPixelate()
    {
        // Prepare
        $expectedBlockSize = 4;
        $expectedMode = true;
        $filterProcessor = FilterProcessor::forPixelate($expectedBlockSize);

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_PIXELATE,
            $expectedBlockSize,
            $expectedMode
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForColorize()
    {
        // Prepare
        $expectedColor = ImageColor::black();
        $filterProcessor = FilterProcessor::forColorize($expectedColor);

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_COLORIZE,
            $expectedColor->getRed(),
            $expectedColor->getGreen(),
            $expectedColor->getBlue(),
            $expectedColor->getAlpha()
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForBrightness()
    {
        // Prepare
        $expectedBrightness = -250;
        $filterProcessor = FilterProcessor::forBrightness($expectedBrightness);

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_BRIGHTNESS,
            $expectedBrightness
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForEdgeDetection()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forEdgeDetection();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_EDGEDETECT
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForContrast()
    {
        // Prepare
        $expectedContrast = -50;
        $filterProcessor = FilterProcessor::forContrast($expectedContrast);

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_CONTRAST,
            $expectedContrast
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForEmboss()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forEmboss();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_EMBOSS
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForGrayscale()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forGrayscale();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_GRAYSCALE
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testForMeanRemoval()
    {
        // Prepare
        $filterProcessor = FilterProcessor::forMeanRemoval();

        // Expect
        $this->globalProphecy->imagefilter(
            $this->image->getHandle(),
            IMG_FILTER_MEAN_REMOVAL
        )->shouldBeCalled()->willReturn(true);

        $this->globalProphecy->reveal();

        // Execute
        $filterProcessor->process($this->image);
    }
}
