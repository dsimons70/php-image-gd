<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\CropProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\CropProcessor
 */
class CropProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testProcessImage()
    {
        // Prepare
        $position = Position::fromInt(10, 10);
        $dimension = Dimension::fromInt(100, 100);

        $rectangle = [
            'x' => $position->getX(),
            'y' => $position->getY(),
            'width' => $dimension->getWidth(),
            'height' => $dimension->getHeight()
        ];

        // Expect
        $this->globalProphecy->imagecrop($this->image->getHandle(), $rectangle)
            ->willReturn(imagecrop($this->image->getHandle(), $rectangle))
            ->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $croppedImage = CropProcessor::fromDimension($dimension, $position)->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $croppedImage);
        self::assertEquals($dimension, $croppedImage->getDimension());
        self::assertNotSame($croppedImage->getHandle(), $this->image->getHandle());
    }
}
