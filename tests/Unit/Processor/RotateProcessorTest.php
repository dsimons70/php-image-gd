<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\RotateProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\RotateProcessor
 */
class RotateProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     *
     * If we try to mocked imagerotate ($this->globalProphecy->imagerotate), the function fails and returns false.
     * As an fallback we test only for valid Image instance
     */
    public function testRotate()
    {
        // Prepare
        $expectedAngle = 90;
        $expectedColor = ImageColor::black();

        // Expect
        $this->globalProphecy->imagerotate(
            $this->image->getHandle(),
            $expectedAngle,
            $this->image->allocateColor($expectedColor)
        )->shouldBeCalled()->willReturn(
            imagerotate(
                $this->image->getHandle(),
                $expectedAngle,
                $this->image->allocateColor($expectedColor)
            )
        );

        $this->globalProphecy->reveal();

        // Execute
        $rotatedImage = RotateProcessor::fromAngle($expectedAngle, $expectedColor)->process($this->image);

        // Assert
        self::assertNotSame($rotatedImage->getHandle(), $this->image->getHandle());
        self::assertInstanceOf(Image::class, $rotatedImage);
    }
}
