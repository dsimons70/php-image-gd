<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\FillProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\FillProcessor
 */
class FillProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testProcessWithoutPosition()
    {
        // Prepare
        $color = ImageColor::white();
        $expected = $this->image->allocateColor($color);

        // Expect
        $this->globalProphecy->imagefill(
            $this->image->getHandle(),
            0,
            0,
            $expected
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $filledImage = FillProcessor::fromColor($color::white())->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $filledImage);
        self::assertSame($filledImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testProcessWithPosition()
    {
        // Prepare
        $color = ImageColor::white();
        $position = Position::fromInt(10, 15);
        $expected = $this->image->allocateColor($color);

        // Expect
        $this->globalProphecy->imagefill(
            $this->image->getHandle(),
            $position->getX(),
            $position->getY(),
            $expected
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $filledImage = FillProcessor::fromColor(ImageColor::white(), $position)->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $filledImage);
        self::assertSame($filledImage->getHandle(), $this->image->getHandle());
    }
}
