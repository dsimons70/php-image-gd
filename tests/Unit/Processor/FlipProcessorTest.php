<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\FlipProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\FlipProcessor
 */
class FlipProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     */
    public function testForHorizontal()
    {
        // Expect
        $this->globalProphecy->imageflip(
            $this->image->getHandle(),
            IMG_FLIP_HORIZONTAL
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $flipImage = FlipProcessor::forHorizontal()->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $flipImage);
        self::assertSame($flipImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testForVertical()
    {
        // Expect
        $this->globalProphecy->imageflip(
            $this->image->getHandle(),
            IMG_FLIP_VERTICAL
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $flipImage = FlipProcessor::forVertical()->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $flipImage);
        self::assertSame($flipImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testForBoth()
    {
        // Expect
        $this->globalProphecy->imageflip(
            $this->image->getHandle(),
            IMG_FLIP_BOTH
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $flipImage = FlipProcessor::forBoth()->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $flipImage);
        self::assertSame($flipImage->getHandle(), $this->image->getHandle());
    }
}
