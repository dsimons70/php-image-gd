<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\SharpenProcessor;
use phpmock\prophecy\PHPProphet;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

/**
 * @covers \Goforit\ImageGD\Processor\SharpenProcessor
 */
class SharpenProcessorTest extends TestCase
{
    private Image $image;
    private PHPProphet $globalProphet;
    private ProphecyInterface $globalProphecy;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->globalProphet = new PHPProphet();
        $this->globalProphecy = $this->globalProphet->prophesize('\Goforit\ImageGD\Processor');
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @inheritdoc
     */
    public function tearDown(): void
    {
        $this->globalProphet->checkPredictions();
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testFromMatrix()
    {
        // Prepare
        $expectedMatrix = [
            [-1.5, -1.0, -1.0],
            [-1.0, 14.0, -1.0],
            [-1.0, -1.0, -1.0],
        ];

        $expectedDivisor = array_sum(array_map('array_sum', $expectedMatrix));

        // Expect
        $this->globalProphecy->imageconvolution(
            $this->image->getHandle(),
            $expectedMatrix,
            $expectedDivisor,
            0
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $processor = SharpenProcessor::fromMatrix($expectedMatrix);
        $processor->process($this->image);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function testFromPercentage()
    {
        // Prepare
        $expectedMatrix = [
            [-1.0, -1.0, -1.0],
            [-1.0, 20.0, -1.0],
            [-1.0, -1.0, -1.0],
        ];

        $givenPercentage = 50;
        $expectedDivisor = array_sum(array_map('array_sum', $expectedMatrix));

        // Expect
        $this->globalProphecy->imageconvolution(
            $this->image->getHandle(),
            $expectedMatrix,
            $expectedDivisor,
            0
        )->shouldBeCalled();

        $this->globalProphecy->reveal();

        // Execute
        $processor = SharpenProcessor::fromPercentage($givenPercentage);
        $processor->process($this->image);
    }
}
