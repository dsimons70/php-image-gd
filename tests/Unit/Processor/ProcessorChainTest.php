<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\Processor;
use Goforit\ImageGD\Processor\ProcessorChain;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Processor\ProcessorChain
 */
class ProcessorChainTest extends TestCase
{
    use ProphecyTrait;

    private ProcessorChain $processorChain;
    private Image $expectedImage;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->expectedImage = Image::forTrueColor(100, 100);
        $this->processorChain = new ProcessorChain();
    }

    /**
     * @test
     */
    public function testProcess(): void
    {
        // Prepare
        $this->processorChain->addProcessor($this->mockProcessor()->reveal());
        $this->processorChain->addProcessor($this->mockProcessor()->reveal());
        $this->processorChain->addProcessor($this->mockProcessor()->reveal());

        // Execute
        $image = $this->processorChain->process($this->expectedImage);

        // Assert
        self::assertInstanceOf(Image::class, $image);
    }

    private function mockProcessor(): ObjectProphecy
    {
        /** @var Processor|ObjectProphecy $processor */
        $processor = $this->prophesize(Processor::class);
        $processor->process($this->expectedImage)
            ->shouldBeCalled()
            ->willReturn($this->expectedImage);

        return $processor;
    }
}
