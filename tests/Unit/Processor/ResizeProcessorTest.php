<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\ResizeProcessor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Goforit\ImageGD\Processor\ResizeProcessor
 */
class ResizeProcessorTest extends TestCase
{
    private Image $image;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->image = Image::forTrueColor(200, 200);
    }

    /**
     * @test
     */
    public function testProcessResizeDimensionAbsolute()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(40, 50);

        // Execute
        $resizedImage = ResizeProcessor::fromDimension($expectedDimension)->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $resizedImage);
        self::assertEquals($expectedDimension, $resizedImage->getDimension());
        self::assertNotSame($resizedImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testProcessResizeDimensionProportional()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(50, 50);

        // Execute
        $resizedImage = ResizeProcessor::fromDimensionProportional(Dimension::fromInt(100, 50))->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $resizedImage);
        self::assertEquals($expectedDimension, $resizedImage->getDimension());
        self::assertNotSame($resizedImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testResizeWidthProportional()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(90, 90);

        // Execute
        $resizedImage = ResizeProcessor::fromWidth($expectedDimension->getWidth())->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $resizedImage);
        self::assertEquals($expectedDimension, $resizedImage->getDimension());
        self::assertNotSame($resizedImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testResizeHeightProportional()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(90, 90);

        // Execute
        $resizedImage = ResizeProcessor::fromHeight($expectedDimension->getHeight())->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $resizedImage);
        self::assertEquals($expectedDimension, $resizedImage->getDimension());
        self::assertNotSame($resizedImage->getHandle(), $this->image->getHandle());
    }

    /**
     * @test
     */
    public function testResizePercentage()
    {
        // Prepare
        $expectedDimension = Dimension::fromInt(100, 100);

        // Execute
        $resizedImage = ResizeProcessor::fromPercentage(50)->process($this->image);

        // Assert
        self::assertInstanceOf(Image::class, $resizedImage);
        self::assertEquals($expectedDimension, $resizedImage->getDimension());
        self::assertNotSame($resizedImage->getHandle(), $this->image->getHandle());
    }
}
