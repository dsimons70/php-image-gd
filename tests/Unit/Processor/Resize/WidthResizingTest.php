<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Processor\Resize\WidthResizing;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Processor\Resize\WidthResizing
 */
class WidthResizingTest extends TestCase
{
    use ProphecyTrait;

    private WidthResizing $widthResize;
    private int $expectedWidth = 100;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->widthResize = WidthResizing::fromInt($this->expectedWidth);
    }

    /**
     * @test
     */
    public function testResize()
    {
        // Prepare
        /** @var Dimension|ObjectProphecy $givenDimension */
        $givenDimension = $this->prophesize(Dimension::class);
        $expectedDimension = Dimension::fromInt(100, 100);

        // Expect
        $givenDimension->resizeWidth($this->expectedWidth)->shouldBeCalled()->willReturn($expectedDimension);

        // Execute
        $result = $this->widthResize->resize($givenDimension->reveal());

        // Assert
        self::assertSame($expectedDimension, $result);
    }
}
