<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Processor\Resize\HeightResizing;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Processor\Resize\HeightResizing
 */
class HeightResizingTest extends TestCase
{
    use ProphecyTrait;

    private HeightResizing $heightResize;
    private int $expectedHeight = 100;

    public function setUp(): void
    {
        $this->heightResize = HeightResizing::fromInt($this->expectedHeight);
    }

    /**
     * @test
     */
    public function testResize()
    {
        // Prepare
        /** @var Dimension|ObjectProphecy $givenDimension */
        $givenDimension = $this->prophesize(Dimension::class);
        $expectedDimension = Dimension::fromInt(100, 100);

        // Expect
        $givenDimension->resizeHeight($this->expectedHeight)->shouldBeCalled()->willReturn($expectedDimension);

        // Execute
        $result = $this->heightResize->resize($givenDimension->reveal());

        // Assert
        self::assertSame($expectedDimension, $result);
    }
}
