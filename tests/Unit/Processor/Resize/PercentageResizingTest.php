<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Processor\Resize\PercentageResizing;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Processor\Resize\PercentageResizing
 */
class PercentageResizingTest extends TestCase
{
    use ProphecyTrait;

    private PercentageResizing $percentageResizing;
    private int $expectedPercentage = 50;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->percentageResizing = PercentageResizing::fromPercentage($this->expectedPercentage);
    }

    /**
     * @test
     */
    public function testResize()
    {
        // Prepare
        /** @var Dimension|ObjectProphecy $givenDimension */
        $givenDimension = $this->prophesize(Dimension::class);
        $expectedDimension = Dimension::fromInt(50, 50);

        // Expect
        $givenDimension->resizePercental($this->expectedPercentage)->shouldBeCalled()->willReturn($expectedDimension);

        // Execute
        $result = $this->percentageResizing->resize($givenDimension->reveal());

        // Assert
        self::assertSame($expectedDimension, $result);
    }
}
