<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Processor\Resize\DimensionResizing;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \Goforit\ImageGD\Processor\Resize\DimensionResizing
 */
class DimensionResizingTest extends TestCase
{
    use ProphecyTrait;

    private Dimension|ObjectProphecy $givenDimension;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        $this->givenDimension = $this->prophesize(Dimension::class);
    }

    /**
     * @test
     */
    public function testResizeAbsolute()
    {
        // Prepare
        /** @var Dimension|ObjectProphecy $expectedDimension */
        $expectedDimension = $this->givenDimension->reveal();
        $dimensionResize = DimensionResizing::fromDimension($expectedDimension, true);

        // Expected
        $this->givenDimension->resizeInto(Argument::any())->shouldNotBeCalled();

        // Execute
        $result = $dimensionResize->resize($this->givenDimension->reveal());

        // Assert
        self::assertSame($expectedDimension, $result);
    }

    /**
     * @test
     */
    public function testResizeProportional()
    {
        // Prepare
        /** @var Dimension|ObjectProphecy $expectedDimension */
        $expectedDimension = Dimension::fromInt(100, 100);
        $dimensionResize = DimensionResizing::fromDimension($this->givenDimension->reveal(), false);

        // Expected
        $this->givenDimension->resizeInto($this->givenDimension->reveal())
            ->shouldBeCalled()
            ->willReturn($expectedDimension);

        // Execute
        $result = $dimensionResize->resize($this->givenDimension->reveal());

        // Assert
        self::assertSame($expectedDimension, $result);
    }
}
