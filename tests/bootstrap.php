<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * @author Dietmar Simons <dietmar.simons@gofor-it.de>
 * @copyright 2018 GOFOR-IT Dietmar Simons
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('zend.assertions', 0);

ini_set('memory_limit', '1024m');

require dirname(__DIR__) . '/vendor/autoload.php';
