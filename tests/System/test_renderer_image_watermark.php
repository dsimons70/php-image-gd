<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Renderer\Image\ImageRenderer;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

$image = Image::fromFile('../Fixtures/images/bladerunner_art.jpg');

$image->applyRenderer(
    ImageRenderer::fromImage(Image::fromFile('../Fixtures/images/deckard.jpg')->setTransparency(80)),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

HttpWriter::create()->writeJpg($image);
