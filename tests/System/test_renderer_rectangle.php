<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

require '../../vendor/autoload.php';

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Renderer\Rectangle\Rectangle;
use Goforit\ImageGD\Renderer\Rectangle\RectangleRenderer;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\FillProcessor;
use Goforit\ImageGD\Writer\HttpWriter;

$image = FillProcessor::fromColor(ImageColor::white())->process(Image::forTrueColor(500, 215));

$rectangle       = Rectangle::fromDimension(Dimension::fromInt(100, 100));
$filledRectangle = Rectangle::fromDimension(Dimension::fromInt(150, 150), true);

$image->applyRenderer(
    RectangleRenderer::fromRectangle($rectangle, ImageColor::black()),
    HorizontalAlignment::left('5px'),
    VerticalAlignment::top('5px')
);

$image->applyRenderer(
    RectangleRenderer::fromRectangle($rectangle, ImageColor::black()),
    HorizontalAlignment::right('5px'),
    VerticalAlignment::top('5px')
);

$image->applyRenderer(
    RectangleRenderer::fromRectangle($rectangle, ImageColor::black()),
    HorizontalAlignment::left('5px'),
    VerticalAlignment::bottom('5px')
);

$image->applyRenderer(
    RectangleRenderer::fromRectangle($rectangle, ImageColor::black()),
    HorizontalAlignment::right('5px'),
    VerticalAlignment::bottom('5px')
);

$image->applyRenderer(
    RectangleRenderer::fromRectangle($filledRectangle, ImageColor::black()),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

HttpWriter::create()->writeJpg($image);
