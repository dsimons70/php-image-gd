<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFont;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFontRenderer;
use Goforit\ImageGD\Renderer\Image\ImageRenderer;
use Goforit\ImageGD\Renderer\RendererChain;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

class CopyrightWithWatermark extends RendererChain
{
    private function __construct(string $year)
    {
        $font = FreeTypeFont::fromFontFile('../Fixtures//fonts/BladeRunner.ttf', 12);

        $this->addRenderer(
            ImageRenderer::fromImage(Image::fromFile('../Fixtures/images/deckard.jpg')),
            HorizontalAlignment::center(),
            VerticalAlignment::middle()
        );

        $this->addRenderer(
            FreeTypeFontRenderer::fromFont($font, ImageColor::white(), "(C) $year Deckard inc"),
            HorizontalAlignment::right('10px'),
            VerticalAlignment::bottom('10px')
        );
    }

    public static function fromYear(string $year): self
    {
        return new self($year);
    }
}

$image     = Image::fromFile('../Fixtures/images/bladerunner_art.jpg');
$copyright = CopyrightWithWatermark::fromYear(date('Y'));

HttpWriter::create()->writeJpg($copyright->render($image));
