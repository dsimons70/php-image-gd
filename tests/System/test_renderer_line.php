<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

require '../../vendor/autoload.php';

use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Renderer\Line\Line;
use Goforit\ImageGD\Renderer\Line\LineRenderer;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\FillProcessor;
use Goforit\ImageGD\Writer\HttpWriter;

$image          = FillProcessor::fromColor(ImageColor::white())->process(Image::forTrueColor(500, 215));
$verticalLine   = Line::forVerticalAlign(160, 6);
$horizontalLine = Line::forHorizontalAlign(160, 6);
$customLine     = Line::fromPositions(Position::fromInt(0, 125), Position::fromInt(125, 0), 1);
$blackColor     = ImageColor::black();

$image->applyRenderer(
    LineRenderer::fromLine($horizontalLine, $blackColor),
    HorizontalAlignment::left('1px'),
    VerticalAlignment::top('1px')
);

$image->applyRenderer(
    LineRenderer::fromLine($horizontalLine, $blackColor),
    HorizontalAlignment::right('1px'),
    VerticalAlignment::top('1px')
);

$image->applyRenderer(
    LineRenderer::fromLine($verticalLine, $blackColor),
    HorizontalAlignment::left('1px'),
    VerticalAlignment::bottom('1px')
);

$image->applyRenderer(
    LineRenderer::fromLine($verticalLine, $blackColor),
    HorizontalAlignment::right('1px'),
    VerticalAlignment::bottom('1px')
);

$image->applyRenderer(
    LineRenderer::fromLine($customLine, $blackColor),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

HttpWriter::create()->writeJpg($image);
