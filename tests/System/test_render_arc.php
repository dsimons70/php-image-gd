<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\FillProcessor;
use Goforit\ImageGD\Renderer\Arc\Arc;
use Goforit\ImageGD\Renderer\Arc\ArcRenderer;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

$image = FillProcessor::fromColor(ImageColor::white())->process(Image::forTrueColor(500, 300));
$arc1  = Arc::forArc(Dimension::fromInt(60, 60), 135, 180);
$arc2  = Arc::forCircleOrEllipse(Dimension::fromInt(60, 60), 1);
$arc3  = Arc::forFilledCircleOrEllipse(Dimension::fromInt(50, 50));
$arc4  = Arc::forPie(Dimension::fromInt(60, 60), 270, 180, 1);
$arc5  = Arc::forFilledPie(Dimension::fromInt(60, 60), 90, 90);

$image->applyRenderer(
    ArcRenderer::fromArc($arc1, ImageColor::black()),
    HorizontalAlignment::left('10px'),
    VerticalAlignment::top('10px')
);

$image->applyRenderer(
    ArcRenderer::fromArc($arc2, ImageColor::black()),
    HorizontalAlignment::right('10px'),
    VerticalAlignment::top('10px')
);

$image->applyRenderer(
    ArcRenderer::fromArc($arc3, ImageColor::black()),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

$image->applyRenderer(
    ArcRenderer::fromArc($arc4, ImageColor::black()),
    HorizontalAlignment::right('10px'),
    VerticalAlignment::bottom('10px')
);

$image->applyRenderer(
    ArcRenderer::fromArc($arc5, ImageColor::black()),
    HorizontalAlignment::left('10px'),
    VerticalAlignment::bottom('10px')
);




HttpWriter::create()->writeJpg($image);
