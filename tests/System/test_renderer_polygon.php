<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

require '../../vendor/autoload.php';

use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Renderer\Polygon\Polygon;
use Goforit\ImageGD\Renderer\Polygon\PolygonRenderer;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Processor\FillProcessor;
use Goforit\ImageGD\Writer\HttpWriter;

$image = FillProcessor::fromColor(ImageColor::white())->process(Image::forTrueColor(500, 215));

$polygon = new Polygon();
$polygon->addPosition(Position::fromInt(0, 49));
$polygon->addPosition(Position::fromInt(49, 0));
$polygon->addPosition(Position::fromInt(99, 0));
$polygon->addPosition(Position::fromInt(49, 99));

$image->applyRenderer(
    PolygonRenderer::fromPolygon($polygon, ImageColor::fromHex('#990099'), true),
    HorizontalAlignment::left('15px'),
    VerticalAlignment::top('15px')
);

$image->applyRenderer(
    PolygonRenderer::fromPolygon($polygon, ImageColor::fromHex('#990099'), true),
    HorizontalAlignment::right('15px'),
    VerticalAlignment::top('15px')
);

$image->applyRenderer(
    PolygonRenderer::fromPolygon($polygon, ImageColor::fromHex('#990099'), true),
    HorizontalAlignment::left('15px'),
    VerticalAlignment::bottom('15px')
);

$image->applyRenderer(
    PolygonRenderer::fromPolygon($polygon, ImageColor::fromHex('#990099'), true),
    HorizontalAlignment::right('15px'),
    VerticalAlignment::bottom('15px')
);

$image->applyRenderer(
    PolygonRenderer::fromPolygon($polygon, ImageColor::fromHex('#990099')),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

HttpWriter::create()->writeJpg($image);
