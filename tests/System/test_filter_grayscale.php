<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\FilterProcessor;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

$image = Image::fromFile('../Fixtures/images/mosel.jpg');

HttpWriter::create()->writeJpg(
    FilterProcessor::forGrayscale()->process($image)
);

