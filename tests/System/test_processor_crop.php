<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\CropProcessor;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

$image = Image::fromFile('../Fixtures/images/f95-fcb.jpg');

HttpWriter::create()->writePng(
    CropProcessor::fromDimension(Dimension::fromInt(320, 130), Position::fromInt(80, 15))->process($image)
);
