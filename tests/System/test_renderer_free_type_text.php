<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Tests\System;

use Goforit\ImageGD\Align\Horizontal\LeftAlign;
use Goforit\ImageGD\Align\HorizontalAlignment;
use Goforit\ImageGD\Align\Vertical\TopAlign;
use Goforit\ImageGD\Align\VerticalAlignment;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFont;
use Goforit\ImageGD\Renderer\FreeTypeFont\FreeTypeFontRenderer;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Writer\HttpWriter;

require '../../vendor/autoload.php';

$image             = Image::fromFile('../Fixtures/images/bladerunner_art.jpg');
$bladeRunnerFont16 = FreeTypeFont::fromFontFile('../Fixtures//fonts/BladeRunner.ttf', 16);
$bladeRunnerFont38 = FreeTypeFont::fromFontFile('../Fixtures//fonts/BladeRunner.ttf', 38);
$whiteColor        = ImageColor::fromHex('#FFFFFF');

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont16, $whiteColor, 'bladerunner 1'),
    LeftAlign::fromString('5px'),
    TopAlign::fromString('5px')
);

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont16, $whiteColor, 'bladerunner 2'),
    HorizontalAlignment::right('5px'),
    VerticalAlignment::top('5px')
);

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont16, $whiteColor, 'bladerunner 3'),
    HorizontalAlignment::left('5px'),
    VerticalAlignment::bottom('5px')
);

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont16, $whiteColor, 'bladerunner 4'),
    HorizontalAlignment::right('5px'),
    VerticalAlignment::bottom('5px')
);

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont38, $whiteColor, 'Finnian MacManus'),
    HorizontalAlignment::center(),
    VerticalAlignment::middle()
);

$image->applyRenderer(
    FreeTypeFontRenderer::fromFont($bladeRunnerFont16, $whiteColor, 'B'),
    HorizontalAlignment::left('5%'),
    VerticalAlignment::middle()
);

HttpWriter::create()->writeJpg($image);
