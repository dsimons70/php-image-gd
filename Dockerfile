FROM php:8.1.13-fpm-alpine

RUN apk add --no-cache --virtual build-deps $PHPIZE_DEPS freetype-dev libpng-dev libjpeg-turbo-dev \
    && apk add --no-cache curl freetype libjpeg-turbo libpng icu-dev \
    && docker-php-source extract \
    && pecl install xdebug-3.1.6 \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-source delete \
    && echo "xdebug.mode = debug,coverage" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host = host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_port = 9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request = trigger" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \

# Composer
RUN apk add curl \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /tmp/*


