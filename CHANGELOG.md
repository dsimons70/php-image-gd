# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.4.0 - 2022-12-25
### Added
- Add support for php 8.1 
### Changed
- Coding standard from PSR 2 to PSR 12

## 0.3.1 - 2019-01-22
### Fixed
- Correct invalid 'composer.jsom' for 'key mikey179/vfsstream'

## 0.3.0 - 2019-01-22
### Added 
- Added renderer for circle, ellipse, chart pie or arc components   
- Changed line renderer to support imageantialias  


## 0.2.0 - 2019-01-16
### Added 
- Added processor for all supported GD filter functions such as grayscale,   
  brightness, edge detection and contrast.
- Renamed system tests to allow grouping based on tested class type  

## 0.1.0 - 2019-01-14
### Added 
- Added tag 0.1.0
- Initial release
