<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD;

use Exception;
use GdImage;
use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Horizontal\HorizontalAlign;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Align\Vertical\VerticalAlign;
use Goforit\ImageGD\Renderer\Renderer;
use SplFileObject;

/**
 * Class to store and manage the image resource handle
 */
class Image
{
    /** @var int[] */
    private array $allocatedColors;
    private GdImage $imageHandle;
    private Dimension $dimension;
    private ?int $transparency = null;

    /** Supported image extensions */
    public const EXTENSION_JPG = 'jpg';
    public const EXTENSION_JPEG = 'jpeg';
    public const EXTENSION_PNG = 'png';
    public const EXTENSION_GIF = 'gif';
    public const EXTENSION_WBMP = 'wbmp';
    public const EXTENSION_XBM = 'xbm';

    private function __construct(GdImage $handle)
    {
        $this->imageHandle = $handle;
        $this->dimension = $this->calculateDimension();
    }

    /**
     * Destructor to free any memory associated with the image at runtime
     */
    public function __destruct()
    {
        imagedestroy($this->imageHandle);
    }

    /**
     * Applies the passed renderer according to the given alignments onto this image
     */
    public function applyRenderer(Renderer $renderer, HorizontalAlign $horizontal, VerticalAlign $vertical): Image
    {
        $topLeftX = $horizontal->getTopLeftX($this->dimension, $renderer->getDimension());
        $topLeftY = $vertical->getTopLeftY($this->dimension, $renderer->getDimension());

        $renderer->render($this, Position::fromInt($topLeftX, $topLeftY));

        return $this;
    }

    /**
     * Allocates the given color only once and returns the image color handle
     */
    public function allocateColor(ImageColor $color): int
    {
        $colorHash = $color->__toString();

        // Allocate each color only once!
        if (isset($this->allocatedColors[$colorHash])) {
            return $this->allocatedColors[$colorHash];
        }

        if ($color->hasAlphaTransparency()) {
            $this->allocatedColors[$colorHash] = imagecolorallocatealpha(
                $this->imageHandle,
                $color->getRed(),
                $color->getGreen(),
                $color->getBlue(),
                $color->getAlpha()
            );
        } else {
            $this->allocatedColors[$colorHash] = imagecolorallocate(
                $this->imageHandle,
                $color->getRed(),
                $color->getGreen(),
                $color->getBlue()
            );
        }

        return $this->allocatedColors[$colorHash];
    }

    /**
     * Returns the transparency value in a range from 0 to 100
     */
    public function getTransparency(): ?int
    {
        return $this->transparency;
    }

    /**
     * Set a transparency value in a range from 0 to 100
     */
    public function setTransparency(int $transparency): Image
    {
        $isValidTransparency = $transparency >= 0 && $transparency <= 100;
        assert($isValidTransparency, 'transparency is invalid');

        if (!$isValidTransparency) {
            return $this;
        }

        $this->transparency = $transparency;

        return $this;
    }

    /**
     * Returns the image gd resource handle
     */
    public function getHandle(): GdImage
    {
        return $this->imageHandle;
    }

    /**
     * Returns the max dimension (bounding box) of this image
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }

    /**
     * Named constructor to create a new palette color based image
     */
    public static function forPalette(int $width, int $height): Image
    {
        return new self(imagecreate($width, $height));
    }

    /**
     * Named constructor to create a new true color based image
     */
    public static function forTrueColor(int $width, int $height): Image
    {
        return new self(imagecreatetruecolor($width, $height));
    }

    /**
     * Named constructor to create an image, based on the given image file pathname
     * @throws ImageException
     */
    public static function fromFile(string $file): Image
    {
        try {
            $fileObject = new SplFileObject($file);
        } catch (Exception $exception) {
            throw new ImageException($exception->getMessage());
        }

        return self::fromFileObject($fileObject);
    }

    /**
     * Named constructor to create an image based on the passed image SplFileObject
     * @throws ImageException
     */
    public static function fromFileObject(SplFileObject $fileObject): Image
    {
        if (!$fileObject->isReadable()) {
            throw ImageException::forNotReadableFiles($fileObject->getPathname());
        }

        switch (strtolower($fileObject->getExtension())) {
            case Image::EXTENSION_PNG:
                $handle = imagecreatefrompng($fileObject->getPathname());
                break;
            case Image::EXTENSION_GIF:
                $handle = imagecreatefromgif($fileObject->getPathname());
                break;
            case Image::EXTENSION_JPG:
            case Image::EXTENSION_JPEG:
                $handle = imagecreatefromjpeg($fileObject->getPathname());
                break;
            case Image::EXTENSION_XBM:
                $handle = imagecreatefromxbm($fileObject->getPathname());
                break;
            case Image::EXTENSION_WBMP:
                $handle = imagecreatefromwbmp($fileObject->getPathname());
                break;
            default:
                throw ImageException::forNotSupportedExtensions($fileObject->getExtension());
        }

        if (!$handle instanceof GdImage) {
            throw ImageException::forNotProcessableFiles($fileObject->getPathname());
        }

        return new self($handle);
    }

    /**
     * Named constructor to create an image based on the passed image string
     * @throws ImageException
     */
    public static function fromString(string $image): Image
    {
        return new self(imagecreatefromstring($image));
    }

    /**
     * Named constructor to create an image based on the passed resource handle
     * @throws ImageException
     */
    public static function fromHandle($handle): Image
    {
        return new self($handle);
    }

    /**
     * Returns the calculated max dimension of this image
     */
    private function calculateDimension(): Dimension
    {
        return Dimension::fromInt(
            imagesx($this->imageHandle),
            imagesy($this->imageHandle)
        );
    }
}
