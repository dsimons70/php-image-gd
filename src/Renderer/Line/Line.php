<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Line;

use Goforit\ImageGD\Align\Position;

/**
 * Value object for a horizontal or vertical Line component.
 */
class Line
{
    private int $thickness;
    private Position $startPosition;
    private Position $endPosition;

    private function __construct(Position $startPosition, Position $endPosition, int $thickness = 1)
    {
        $isValidThickness = $thickness >= 1;

        assert($isValidThickness, 'thickness must be equal or greater than 1');

        if (!$isValidThickness) {
            $thickness = 1;
        }

        $this->startPosition = $startPosition;
        $this->endPosition = $endPosition;
        $this->thickness = $thickness;
    }

    /**
     * Named Constructor to create an instance based on the given start and end Position
     */
    public static function fromPositions(Position $startPosition, Position $endPosition, int $thickness = 1): Line
    {
        return new self($startPosition, $endPosition, $thickness);
    }

    /**
     * Named constructor for a horizontal Line with the given width.
     */
    public static function forHorizontalAlign(int $width, int $thickness): Line
    {
        $isValidWidth = $width > 0;

        assert($isValidWidth, 'Width must be equal or greater than 1');

        if (!$isValidWidth) {
            $width = 100;
        }

        $startPosition = Position::fromInt(0, 0);
        $endPosition = Position::fromInt($width - 1, 0);

        return new self($startPosition, $endPosition, $thickness);
    }

    /**
     * Named constructor for a vertical Line based on the given height.
     */
    public static function forVerticalAlign(int $height, int $thickness): Line
    {
        $isValidHeight = $height > 0;

        assert($isValidHeight, 'Height must be equal or greater than 1');

        if (!$isValidHeight) {
            $height = 100;
        }

        $startPosition = Position::fromInt(0, 0);
        $endPosition = Position::fromInt(0, $height - 1);

        return new self($startPosition, $endPosition, $thickness);
    }

    /**
     * Return the angle.
     */
    public function getAngle(): float
    {
        $deltaY = $this->endPosition->getY() - $this->startPosition->getY();
        $deltaX = $this->endPosition->getX() - $this->startPosition->getX();

        return atan2($deltaY, $deltaX) * 180 / M_PI;
    }

    /**
     * Return the start Position of the line.
     */
    public function getStartPosition(): Position
    {
        return $this->startPosition;
    }

    /**
     * Returns the end position of the line.
     */
    public function getEndPosition(): Position
    {
        return $this->endPosition;
    }

    /**
     * Returns the thickness of the line.
     */
    public function getThickness(): int
    {
        return $this->thickness;
    }
}
