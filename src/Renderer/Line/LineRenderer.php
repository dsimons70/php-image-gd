<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Line;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render a Line onto the passed Image
 */
class LineRenderer implements Renderer
{
    private ImageColor $color;
    private Line $line;
    private Dimension $dimension;

    private function __construct(Line $line, ImageColor $color)
    {
        $this->color = $color;
        $this->line = $line;
        $this->dimension = $this->calculateDimension($line);
    }

    /**
     * Named constructor to create a renderer for the passed Line
     */
    public static function fromLine(Line $line, ImageColor $color): LineRenderer
    {
        return new self($line, $color);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        imagesetthickness($targetImage->getHandle(), $this->line->getThickness());

        // Antialias works only for 1px lines
        if ($this->line->getThickness() === 1) {
            imageantialias($targetImage->getHandle(), true);
        }

        $thicknessOffset = intval(floor($this->line->getThickness() / 2));

        if ($this->line->getAngle() === 0.0) {
            imageline(
                $targetImage->getHandle(),
                $topLeftPosition->getX() + $this->line->getStartPosition()->getX(),
                $topLeftPosition->getY() + $this->line->getStartPosition()->getY() + $thicknessOffset,
                $topLeftPosition->getX() + $this->line->getEndPosition()->getX(),
                $topLeftPosition->getY() + $this->line->getEndPosition()->getY() + $thicknessOffset,
                $targetImage->allocateColor($this->color)
            );
        } else {
            imageline(
                $targetImage->getHandle(),
                $topLeftPosition->getX() + $this->line->getStartPosition()->getX() + $thicknessOffset,
                $topLeftPosition->getY() + $this->line->getStartPosition()->getY(),
                $topLeftPosition->getX() + $this->line->getEndPosition()->getX() + $thicknessOffset,
                $topLeftPosition->getY() + $this->line->getEndPosition()->getY(),
                $targetImage->allocateColor($this->color)
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }

    /**
     * Returns the calculated max dimension of the given Line
     */
    private function calculateDimension(Line $line): Dimension
    {
        // Depending on the angle we have to calculate different values
        switch ($line->getAngle()) {
            case 0.0:
                $width = $line->getStartPosition()->getX() + $line->getEndPosition()->getX() + 1;
                $height = $line->getThickness();
                break;
            case 90.0:
                $width = $line->getThickness();
                $height = $line->getStartPosition()->getY() + $line->getEndPosition()->getY() + 1;
                break;
            default:
                $width = $line->getStartPosition()->getX() + $line->getEndPosition()->getX();
                $height = $line->getStartPosition()->getY() + $line->getEndPosition()->getY();
        }

        return Dimension::fromInt($width, $height);
    }
}
