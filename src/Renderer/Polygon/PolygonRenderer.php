<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Polygon;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\ImageException;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render a polygon on the given Image, based on the injected Polygon and ImageColor
 */
class PolygonRenderer implements Renderer
{
    private Polygon $polygon;
    private ImageColor $color;
    private bool $isFilled;
    private Dimension $dimension;

    /**
     * @throws ImageException
     */
    private function __construct(Polygon $polygon, ImageColor $color, bool $isFilled = false)
    {
        $this->polygon = $polygon;
        $this->color = $color;
        $this->isFilled = $isFilled;
        $this->dimension = $polygon->getDimension();
    }

    /**
     * Named constructor to create an instance based on the given Polygon
     * @throws ImageException
     */
    public static function fromPolygon(Polygon $polygon, ImageColor $color, bool $isFilled = false): self
    {
        return new self($polygon, $color, $isFilled);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        if ($this->isFilled) {
            imagefilledpolygon(
                $targetImage->getHandle(),
                $this->calculateVertices($topLeftPosition),
                $targetImage->allocateColor($this->color)
            );
        } else {
            imagepolygon(
                $targetImage->getHandle(),
                $this->calculateVertices($topLeftPosition),
                $targetImage->allocateColor($this->color)
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }

    /**
     * Returns the calculated vertices array for the imagepolygon function
     */
    private function calculateVertices(Position $topLeftPosition): array
    {
        $vertices = [];

        /** @var Position $position */
        foreach ($this->polygon as $position) {
            $vertices[] = $position->getX() + $topLeftPosition->getX();
            $vertices[] = $position->getY() + $topLeftPosition->getY();
        }

        return $vertices;
    }
}
