<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Polygon;

use Countable;
use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\ImageException;
use IteratorAggregate;

/**
 * Object to aggregate an array of Position objects to define a Polygon component
 */
class Polygon implements Countable, IteratorAggregate
{
    /** @var Position[] */
    private array $positions = [];

    /**
     * Adds a Position object to Polygon
     *
     * To align the polygon correct, you have to ensure that al least one value of the x and y axis
     * references the value 0. (x=0, Y=0)
     *
     * <code>
     *  $myPolygon = new Polygon();
     *  $myPolygon->addPosition(Position::fromInt(25, 0) // y=0;
     *  $myPolygon->addPosition(Position::fromInt(80, 25));
     *  $myPolygon->addPosition(Position::fromInt(50, 50));
     *  $myPolygon->addPosition(Position::fromInt(0, 50) // $x=0);
     * </code>
     */
    public function addPosition(Position $position): void
    {
        $this->positions[] = $position;
    }

    /**
     * Returns the aggregated max dimension of all added positions
     *
     * Will throw an exception if we have less than three positions. GD imagepolygon needs at least
     * three positions in order to render the polygon. To align the polygon correct, we have to ensure
     * that al least one value of the x and y axis references the value 0. (x=0, Y=0)
     *
     * @throws ImageException
     */
    public function getDimension(): Dimension
    {
        if (!$this->hasEnoughPositions()) {
            throw ImageException::forPolygonWithoutEnoughPositions();
        }

        $allX = [];
        $allY = [];

        foreach ($this->positions as $position) {
            $allX[] = $position->getX();
            $allY[] = $position->getY();
        }

        $hasTopLeftPosition = $this->containsTopLeftPosition($allX, $allY);

        assert($hasTopLeftPosition, 'Positions should reference the top (y=0) and the left (x=0) position');

        $maxWidth = max($allX) - min($allX) + 1;
        $maxHeight = max($allY) - min($allY) + 1;

        return Dimension::fromInt($maxWidth, $maxHeight);
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->positions);
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->positions);
    }

    /**
     * We need at least three positions to create an polygon and to calculate the max dimension
     */
    private function hasEnoughPositions(): bool
    {
        return count($this->positions) >= 3;
    }

    /**
     * To align the polygon correct, we have to ensure that al least one value of the x and y axis
     * references the value 0. (X=0, Y=0)
     */
    private function containsTopLeftPosition(array $allX, array $allY): bool
    {
        return min($allX) === 0 && min($allY) === 0;
    }
}
