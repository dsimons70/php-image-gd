<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;

/**
 * Interface to render components onto the passed target image at the passed position
 */
interface Renderer
{
    /**
     * Returns the Dimension (bounding box) of this renderer component
     */
    public function getDimension(): Dimension;

    /**
     * Renders this component onto the passed Image at the passed Position
     */
    public function render(Image $targetImage, Position $topLeftPosition): void;
}
