<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\FreeTypeFont;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render a FreeTypeFont text onto the passed Image
 */
class FreeTypeFontRenderer implements Renderer
{
    private string $text;
    private ImageColor $color;
    private FreeTypeFont $fontFile;
    private Dimension $dimension;

    private function __construct(string $text, ImageColor $color, FreeTypeFont $fontFile)
    {
        $this->text = $text;
        $this->color = $color;
        $this->fontFile = $fontFile;
        $this->dimension = $this->calculateDimension();
    }

    /**
     * Named constructor to create a FreeTypeFontRenderer based on the passed FreeTypeFont, ImageColor and text
     */
    public static function fromFont(FreeTypeFont $freeTypeFont, ImageColor $color, string $text): FreeTypeFontRenderer
    {
        return new self($text, $color, $freeTypeFont);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        imagefttext(
            $targetImage->getHandle(),
            $this->fontFile->getSize(),
            $this->fontFile->getAngle(),
            $topLeftPosition->getX(),
            $topLeftPosition->getY() + $this->getDimension()->getHeight(),
            $targetImage->allocateColor($this->color),
            $this->fontFile->getFontFile(),
            $this->text
        );
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }

    /**
     * Returns the calculated max dimension of this image component.
     */
    private function calculateDimension(): Dimension
    {
        $boundingBox = imageftbbox(
            $this->fontFile->getSize(),
            $this->fontFile->getAngle(),
            $this->fontFile->getFontFile(),
            $this->text
        );

        $width = abs($boundingBox[0]) + abs($boundingBox[2]);
        $height = abs($boundingBox[1]) + abs($boundingBox[5]);

        return Dimension::fromInt(intval($width), intval($height));
    }
}
