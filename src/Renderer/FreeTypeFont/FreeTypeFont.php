<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\FreeTypeFont;

use Goforit\ImageGD\ImageException;
use SplFileInfo;

/**
 * Value object for a FreeTypeFont component.
 */
class FreeTypeFont
{
    private int $size;
    private string $fontFile;
    private int $angle;

    /**
     * @throws  ImageException
     */
    private function __construct(string $fontFile, int $size, int $angle = 0)
    {
        $fileInfo = new SplFileInfo($fontFile);

        if (!$fileInfo->isReadable()) {
            throw ImageException::forNotReadableFiles($fontFile);
        }

        if (strtolower($fileInfo->getExtension()) !== 'ttf') {
            throw ImageException::forNotSupportedExtensions($fileInfo->getExtension());
        }

        $this->fontFile = $fontFile;
        $this->size = abs($size);
        $this->angle = $angle;
    }

    /**
     * Named constructor to create an instance based on the given .ttf font file.
     * @throws  ImageException
     */
    public static function fromFontFile(string $fontFile, int $size, int $angle = 0): FreeTypeFont
    {
        return new self($fontFile, $size, $angle);
    }

    /**
     * Return the size.
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Return the path to the font file.
     */
    public function getFontFile(): string
    {
        return $this->fontFile;
    }

    /**
     * Return the angle.
     */
    public function getAngle(): int
    {
        return $this->angle;
    }
}
