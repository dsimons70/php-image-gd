<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Image;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render an Image onto the passed Image
 */
class ImageRenderer implements Renderer
{
    private Image $image;
    private Dimension $dimension;

    private function __construct(Image $image)
    {
        $this->image = $image;
        $this->dimension = $image->getDimension();
    }

    /**
     * Named constructor to crate an instance from the given Image
     */
    public static function fromImage(Image $image): ImageRenderer
    {
        return new self($image);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        // Prefer imagecopyresampled for better image quality if no transparency is required
        if ($this->image->getTransparency() === null) {
            imagecopyresampled(
                $targetImage->getHandle(),
                $this->image->getHandle(),
                $topLeftPosition->getX(),
                $topLeftPosition->getY(),
                0,
                0,
                $this->dimension->getWidth(),
                $this->dimension->getHeight(),
                $this->dimension->getWidth(),
                $this->dimension->getHeight()
            );
        } else {
            imagecopymerge(
                $targetImage->getHandle(),
                $this->image->getHandle(),
                $topLeftPosition->getX(),
                $topLeftPosition->getY(),
                0,
                0,
                $this->dimension->getWidth(),
                $this->dimension->getHeight(),
                $this->image->getTransparency()
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->image->getDimension();
    }
}
