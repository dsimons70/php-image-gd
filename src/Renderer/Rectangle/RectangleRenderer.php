<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Rectangle;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render a Rectangle on the given Image, based on the injected ImageColor.
 */
class RectangleRenderer implements Renderer
{
    private ImageColor $color;
    private Rectangle $rectangle;

    private function __construct(Rectangle $rectangle, ImageColor $color)
    {
        $this->color = $color;
        $this->rectangle = $rectangle;
    }

    /**
     * Named constructor to create an instance based on the passed Rectangle.
     */
    public static function fromRectangle(Rectangle $rectangle, ImageColor $imageColor): RectangleRenderer
    {
        return new self($rectangle, $imageColor);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        if ($this->rectangle->isFilled()) {
            imagefilledrectangle(
                $targetImage->getHandle(),
                $topLeftPosition->getX(),
                $topLeftPosition->getY(),
                $topLeftPosition->getX() + $this->rectangle->getDimension()->getWidth() - 1,
                $topLeftPosition->getY() + $this->rectangle->getDimension()->getHeight() - 1,
                $targetImage->allocateColor($this->color)
            );
        } else {
            imagerectangle(
                $targetImage->getHandle(),
                $topLeftPosition->getX(),
                $topLeftPosition->getY(),
                $topLeftPosition->getX() + $this->rectangle->getDimension()->getWidth() - 1,
                $topLeftPosition->getY() + $this->rectangle->getDimension()->getHeight() - 1,
                $targetImage->allocateColor($this->color)
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->rectangle->getDimension();
    }
}
