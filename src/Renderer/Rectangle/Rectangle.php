<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Rectangle;

use Goforit\ImageGD\Align\Dimension;

/**
 * Value object for a Rectangle image component.
 */
class Rectangle
{
    private Dimension $dimension;
    private bool $isFilled;

    private function __construct(Dimension $dimension, bool $isFilled)
    {
        $this->isFilled = $isFilled;
        $this->dimension = $dimension;
    }

    /**
     * Named constructor to create a Rectangle based on the given Dimension.
     */
    public static function fromDimension(Dimension $dimension, bool $isFilled = false): self
    {
        return new self($dimension, $isFilled);
    }

    /**
     * Returns the Dimension.
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }

    /**
     * Returns the bool isFilled.
     */
    public function isFilled(): bool
    {
        return $this->isFilled;
    }
}
