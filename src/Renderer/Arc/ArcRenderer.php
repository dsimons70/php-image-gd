<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Arc;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\Renderer\Renderer;

/**
 * Class to render a circle, ellipse, pie or arc onto the passed Image
 */
class ArcRenderer implements Renderer
{
    private Arc $arc;
    private ImageColor $color;

    private function __construct(Arc $arc, ImageColor $color)
    {
        $this->arc = $arc;
        $this->color = $color;
    }

    /**
     * Named constructor to create an instance based on the passed Arc
     */
    public static function fromArc(Arc $arc, ImageColor $color): ArcRenderer
    {
        return new self($arc, $color);
    }

    /**
     * @inheritdoc
     */
    public function render(Image $targetImage, Position $topLeftPosition): void
    {
        if (!$this->arc->isFilled()) {
            imagesetthickness($targetImage->getHandle(), $this->arc->getThickness());
        }

        imagefilledarc(
            $targetImage->getHandle(),
            $topLeftPosition->getX() + $this->arc->getCenterPosition()->getX(),
            $topLeftPosition->getY() + $this->arc->getCenterPosition()->getY(),
            $this->arc->getDimension()->getWidth() - $this->arc->getThickness(),
            $this->arc->getDimension()->getHeight() - $this->arc->getThickness(),
            $this->arc->getStartDegree(),
            $this->arc->getEndDegree(),
            $targetImage->allocateColor($this->color),
            $this->arc->getFillStyle()
        );
    }

    /**
     * @inheritdoc
     */
    public function getDimension(): Dimension
    {
        return $this->arc->getDimension();
    }
}