<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer\Arc;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;

/**
 * Value object for a circle, ellipse, pie and arc components
 */
class Arc
{
    private Dimension $dimension;
    private int $startDegree;
    private int $endDegree;
    private int $fillStyle;
    private int $thickness;

    private function __construct(
        Dimension $dimension,
        int $start,
        int $end,
        int $thickness,
        int $fillStyle
    ) {
        $this->dimension = $dimension;
        $this->startDegree = $start;
        $this->endDegree = $end;
        $this->thickness = $thickness;
        $this->fillStyle = $fillStyle;
    }

    /**
     * Named constructor to create an instance for an arc
     * The 0° start degree is located at the three-o'clock position, the arc is drawn clockwise
     */
    public static function forArc(Dimension $dimension, int $startDegree, int $angle, int $thickness = 1): Arc
    {
        return new self(
            $dimension,
            $startDegree,
            $startDegree + $angle,
            $thickness,
            IMG_ARC_NOFILL
        );
    }

    /**
     * Named constructor to create an instance for an outlined pie
     * The 0° start degree is located at the three-o'clock position, the arc is drawn clockwise
     */
    public static function forPie(Dimension $dimension, int $startDegree, int $angle, int $thickness = 1): Arc
    {
        return new self(
            $dimension,
            $startDegree,
            $startDegree + $angle,
            $thickness,
            IMG_ARC_NOFILL + IMG_ARC_EDGED
        );
    }

    /**
     * Named constructor to create an instance for a filled pie
     * The 0° start degree is located at the three-o'clock position, the arc is drawn clockwise
     */
    public static function forFilledPie(Dimension $dimension, int $startDegree, int $angle): Arc
    {
        return new self(
            $dimension,
            $startDegree,
            $startDegree + $angle,
            0,
            IMG_ARC_PIE
        );
    }

    /**
     * Named constructor to create an instance for a circle or ellipse
     */
    public static function forCircleOrEllipse(Dimension $dimension, int $thickness = 1): Arc
    {
        return new self(
            $dimension,
            0,
            360,
            $thickness,
            IMG_ARC_NOFILL
        );
    }

    /**
     * Named constructor to create an instance for a filled circle or ellipse
     */
    public static function forFilledCircleOrEllipse(Dimension $dimension): Arc
    {
        return new self(
            $dimension,
            0,
            360,
            0,
            IMG_ARC_ROUNDED
        );
    }

    /**
     * Returns the fill style for the imagefilledarc function
     */
    public function getFillStyle(): int
    {
        return $this->fillStyle;
    }

    /**
     * Returns if the arc, circle or ellipse should be filled with a color
     */
    public function isFilled(): bool
    {
        switch ($this->fillStyle) {
            case IMG_ARC_NOFILL:
            case IMG_ARC_NOFILL + IMG_ARC_EDGED:
                return false;
            default:
                return true;
        }
    }

    /**
     * Return the center position of the arc
     */
    public function getCenterPosition(): Position
    {
        return Position::fromInt(
            (int)round($this->dimension->getWidth() / 2),
            (int)round($this->dimension->getHeight() / 2)
        );
    }

    /**
     * Returns the thickness
     */
    public function getThickness(): int
    {
        return $this->thickness;
    }

    /**
     * Returns the arc end angle in degrees
     * The 0° start degree is located at the three-o'clock position
     */
    public function getEndDegree(): int
    {
        return $this->endDegree;
    }

    /**
     * Returns the arc start angle, in degrees.
     * The 0° start degree is located at the three-o'clock position
     */
    public function getStartDegree(): int
    {
        return $this->startDegree;
    }

    /**
     * Return the Dimension
     */
    public function getDimension(): Dimension
    {
        return $this->dimension;
    }
}