<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Renderer;

use Goforit\ImageGD\Align\Horizontal\HorizontalAlign;
use Goforit\ImageGD\Align\Vertical\VerticalAlign;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageException;

/**
 * Class to chain Renderer
 */
class RendererChain
{
    /** @var Renderer[] */
    private array $renderer;

    /** @var VerticalAlign[] */
    private array $verticalAlignments;

    /** @var HorizontalAlign[] */
    private array $horizontalAlignments;

    /**
     * Add a renderer to the chain
     * @throws ImageException
     */
    public function addRenderer(Renderer $renderer, HorizontalAlign $horizontal, VerticalAlign $vertical): void
    {
        $hash = spl_object_hash($renderer);

        if (isset($this->renderer[$hash])) {
            throw new ImageException("Renderer: '$hash' is already registered");
        }

        $this->renderer[$hash] = $renderer;
        $this->horizontalAlignments[$hash] = $horizontal;
        $this->verticalAlignments[$hash] = $vertical;
    }

    /**
     * Apply all registered renderer onto the given image
     */
    public function render(Image $image): Image
    {
        foreach ($this->renderer as $hash => $renderer) {
            $verticalAlign = $this->verticalAlignments[$hash];
            $horizontalAlign = $this->horizontalAlignments[$hash];

            $image->applyRenderer(
                $renderer,
                $horizontalAlign,
                $verticalAlign
            );
        }

        return $image;
    }
}
