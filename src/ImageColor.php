<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD;

/**
 * Value object to represent all rgb color values including alpha transparency
 */
class ImageColor
{
    private int $red = 0;
    private int $green = 0;
    private int $blue = 0;
    private ?int $alpha = null;

    /**
     * Assertions are used to ensure that each color is in a range from 0 - 255. The alpha
     * transparency is optional (null) or must be in a range from 0 - 127. The default
     * values (black) are used, if one of the provided values are invalid
     */
    private function __construct(int $red, int $green, int $blue, int $alpha = null)
    {
        $isValidRed = $this->isValidColor($red);
        $isValidGreen = $this->isValidColor($green);
        $isValidBlue = $this->isValidColor($blue);
        $isValidAlpha = $this->isValidAlpha($alpha);

        assert($isValidRed, 'Invalid color value for red');
        assert($isValidGreen, 'Invalid color value for green');
        assert($isValidBlue, 'Invalid color value for blue');
        assert($isValidAlpha, 'Invalid alpha transparency value');

        if ($isValidRed && $isValidGreen && $isValidBlue) {
            $this->red = $red;
            $this->green = $green;
            $this->blue = $blue;
        }

        if ($isValidAlpha) {
            $this->alpha = $alpha;
        }
    }

    /**
     * Named constructor to create an instance from RGB values
     */
    public static function fromRgb(int $red, int $green, int $blue, int $alpha = null): ImageColor
    {
        return new self($red, $green, $blue, $alpha);
    }

    /**
     * Named constructor to create an instance from HEX values
     */
    public static function fromHex(string $hexColor, int $alpha = null): ImageColor
    {
        $isValidHexColor = preg_match('/^#([a-fA-F0-9]{3}){1,2}\b$/', $hexColor) === 1;
        assert($isValidHexColor, "Invalid hex color value: $hexColor");

        if (!$isValidHexColor) {
            $hexColor = '#000000';
        }

        // Check for shorthand syntax like #FFF
        if (strlen($hexColor) === 4) {
            $red = (int)hexdec(substr($hexColor, 1, 1) . substr($hexColor, 1, 1));
            $green = (int)hexdec(substr($hexColor, 2, 1) . substr($hexColor, 2, 1));
            $blue = (int)hexdec(substr($hexColor, 3, 1) . substr($hexColor, 3, 1));
        } else {
            $red = (int)hexdec(substr($hexColor, 1, 2));
            $green = (int)hexdec(substr($hexColor, 3, 2));
            $blue = (int)hexdec(substr($hexColor, 5, 2));
        }

        return new self($red, $green, $blue, $alpha);
    }

    /**
     * Named constructor to create an instance for white color
     */
    public static function white(int $alpha = null): ImageColor
    {
        return new self(255, 255, 255, $alpha);
    }

    /**
     * Named constructor to create an instance for black color
     */
    public static function black(int $alpha = null): ImageColor
    {
        return new self(0, 0, 0, $alpha);
    }

    /**
     * Return the red value (R)GBA
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * Return the green value R(G)BA
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * Return the blue value RG(B)A
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * Return the alpha value RGB(A)
     */
    public function getAlpha(): int
    {
        return $this->alpha ?? 0;
    }

    /**
     * Checks for alpha transparency
     **/
    public function hasAlphaTransparency(): bool
    {
        return $this->alpha !== null;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return 'R' . $this->red . 'G' . $this->green . 'B' . $this->blue . 'A' . $this->alpha;
    }

    /**
     * Checks for valid alpha transparency values (null, 0 - 127).
     */
    private function isValidAlpha($alpha): bool
    {
        if ($alpha === null) {
            return true;
        }

        return $alpha >= 0 && $alpha <= 127;
    }

    /**
     * Checks vor valid color values (0 - 255).
     */
    private function isValidColor(int $color): bool
    {
        return $color >= 0 && $color <= 255;
    }
}
