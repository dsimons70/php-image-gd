<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;

/**
 * Class to fill images with color
 */
class FillProcessor implements Processor
{
    private ImageColor $color;
    private ?Position $position;

    private function __construct(ImageColor $color, Position $position = null)
    {
        $this->color = $color;
        $this->position = $position ?? Position::fromInt(0, 0);
    }

    /**
     * Named constructor to fill the image with the passed color at the passed position.
     */
    public static function fromColor(ImageColor $color, Position $position = null): FillProcessor
    {
        return new self($color, $position);
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        imagefill(
            $image->getHandle(),
            $this->position->getX(),
            $this->position->getY(),
            $image->allocateColor($this->color)
        );

        return $image;
    }
}
