<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Align\Position;
use Goforit\ImageGD\Image;

/**
 * Class to crop images
 */
class CropProcessor implements Processor
{
    private Position $position;
    private Dimension $dimension;

    private function __construct(Position $position, Dimension $dimension)
    {
        $this->position = $position;
        $this->dimension = $dimension;
    }

    /**
     * Named constructor to crop the image in the passed dimension at the passed position
     */
    public static function fromDimension(Dimension $dimension, Position $position): CropProcessor
    {
        return new self($position, $dimension);
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        $rectangle = [
            'x' => $this->position->getX(),
            'y' => $this->position->getY(),
            'width' => $this->dimension->getWidth(),
            'height' => $this->dimension->getHeight()
        ];

        return Image::fromHandle(imagecrop($image->getHandle(), $rectangle));
    }
}
