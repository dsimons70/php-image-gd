<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;

/**
 * Class to sharpen images
 */
class SharpenProcessor implements Processor
{
    /**
     * @var array A 3x3 matrix: an array of three arrays of three floats
     * @see http://php.net/manual/en/function.imageconvolution.php
     */
    private array $sharpenMatrix;

    private function __construct(array $sharpenMatrix)
    {
        $this->sharpenMatrix = $sharpenMatrix;
    }

    /**
     * Named constructor to create an instance based on a build in standard configuration
     */
    public static function fromPercentage(int $percentage = 60): SharpenProcessor
    {
        $sharpeningFactor = abs($percentage) / 100;
        $sharpening = 10 / $sharpeningFactor;

        return new self([
            [-1.0, -1.0, -1.0],
            [-1.0, $sharpening, -1.0],
            [-1.0, -1.0, -1.0],
        ]);
    }

    /**
     * Named constructor to create an instance based on the passed matrix array
     * @see http://php.net/manual/en/function.imageconvolution.php
     */
    public static function fromMatrix(array $matrix): SharpenProcessor
    {
        return new self($matrix);
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        $divisor = array_sum(array_map('array_sum', $this->sharpenMatrix));
        imageconvolution($image->getHandle(), $this->sharpenMatrix, $divisor, 0);

        return $image;
    }
}
