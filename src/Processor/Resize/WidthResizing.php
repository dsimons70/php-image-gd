<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;

class WidthResizing implements Resizing
{
    private int $newWidth;

    private function __construct(int $newWidth)
    {
        $this->newWidth = $newWidth;
    }

    /**
     * Named constructor to create an instance based on the given width
     */
    public static function fromInt(int $width): WidthResizing
    {
        return new self($width);
    }

    /**
     * @inheritdoc
     */
    public function resize(Dimension $dimension): Dimension
    {
        return $dimension->resizeWidth($this->newWidth);
    }
}
