<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;

class DimensionResizing implements Resizing
{
    private Dimension $newDimension;
    private bool $isAbsolute;

    private function __construct(Dimension $dimension, bool $isAbsolute)
    {
        $this->newDimension = $dimension;
        $this->isAbsolute = $isAbsolute;
    }

    /**
     * Named constructor to create an instance based on the given Dimension
     *
     * If the second parameter is false, then we resize proportional
     * until the old dimension fits into the passed dimension
     */
    public static function fromDimension(Dimension $newDimension, $isAbsolute = false): DimensionResizing
    {
        return new self($newDimension, $isAbsolute);
    }

    /**
     * @inheritdoc
     */
    public function resize(Dimension $dimension): Dimension
    {
        if ($this->isAbsolute) {
            return $this->newDimension;
        }

        return $dimension->resizeInto($this->newDimension);
    }
}
