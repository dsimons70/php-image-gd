<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;

class PercentageResizing implements Resizing
{
    private int $percentage;

    private function __construct(int $percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * Creates an Instance based on the given factor
     */
    public static function fromPercentage(int $factor): PercentageResizing
    {
        return new self($factor);
    }

    /**
     * @inheritdoc
     */
    public function resize(Dimension $dimension): Dimension
    {
        return $dimension->resizePercental($this->percentage);
    }
}
