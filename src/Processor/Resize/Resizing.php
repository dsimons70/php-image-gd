<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;

/**
 * Interface to allow different resizing strategy's
 */
interface Resizing
{
    /**
     * Resize the given Dimension
     */
    public function resize(Dimension $dimension): Dimension;
}
