<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor\Resize;

use Goforit\ImageGD\Align\Dimension;

class HeightResizing implements Resizing
{
    private int $newHeight;

    private function __construct(int $newHeight)
    {
        $this->newHeight = $newHeight;
    }

    /**
     * Creates an Instance based on the given height
     */
    public static function fromInt(int $height): HeightResizing
    {
        return new self($height);
    }

    /**
     * @inheritdoc
     */
    public function resize(Dimension $dimension): Dimension
    {
        return $dimension->resizeHeight($this->newHeight);
    }
}
