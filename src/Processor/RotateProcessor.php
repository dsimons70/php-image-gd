<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;

/**
 * Class to rotate images
 */
class RotateProcessor implements Processor
{
    private ImageColor $color;
    private float $angle;

    public function __construct(ImageColor $color, float $angle)
    {
        $this->color = $color;
        $this->angle = $angle;
    }

    /**
     * Creates a rotate processor based on the passed angle and background color
     */
    public static function fromAngle(float $angle, ImageColor $color): RotateProcessor
    {
        return new self($color, $angle);
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        $newHandle = imagerotate(
            $image->getHandle(),
            $this->angle,
            $image->allocateColor($this->color)
        );

        return Image::fromHandle($newHandle);
    }
}
