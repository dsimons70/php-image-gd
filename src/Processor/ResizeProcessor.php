<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Align\Dimension;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\Processor\Resize\DimensionResizing;
use Goforit\ImageGD\Processor\Resize\HeightResizing;
use Goforit\ImageGD\Processor\Resize\PercentageResizing;
use Goforit\ImageGD\Processor\Resize\Resizing;
use Goforit\ImageGD\Processor\Resize\WidthResizing;

/**
 * Class to resize images
 */
class ResizeProcessor implements Processor
{
    private Resizing $resizingStrategy;

    private function __construct(Resizing $resizing)
    {
        $this->resizingStrategy = $resizing;
    }

    /**
     * Named constructor to proportionally resize the image based on the passed percentage value
     */
    public static function fromPercentage(int $percentage): ResizeProcessor
    {
        return new self(PercentageResizing::fromPercentage($percentage));
    }

    /**
     * Named constructor to resize the image absolutely, based on the passed new dimension
     */
    public static function fromDimension(Dimension $newDimension): ResizeProcessor
    {
        return new self(DimensionResizing::fromDimension($newDimension, true));
    }

    /**
     * Named constructor to resize the image proportional until it fits into the passed max dimension
     */
    public static function fromDimensionProportional(Dimension $maxDimension): ResizeProcessor
    {
        return new self(DimensionResizing::fromDimension($maxDimension, false));
    }

    /**
     * Named constructor to resize the image proportional, based on the passed width
     */
    public static function fromWidth(int $newWidth): ResizeProcessor
    {
        return new self(WidthResizing::fromInt($newWidth));
    }

    /**
     * Named constructor to resize the image proportional, based on the passed height
     */
    public static function fromHeight(int $newHeight): ResizeProcessor
    {
        return new self(HeightResizing::fromInt($newHeight));
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        $newDimension = $this->resizingStrategy->resize($image->getDimension());
        $newHandle = imagecreatetruecolor($newDimension->getWidth(), $newDimension->getHeight());

        // Preserve transparent background for gif and png
        imagealphablending($newHandle, false);

        imagecopyresampled(
            $newHandle,
            $image->getHandle(),
            0,
            0,
            0,
            0,
            $newDimension->getWidth(),
            $newDimension->getHeight(),
            $image->getDimension()->getWidth(),
            $image->getDimension()->getHeight()
        );

        return Image::fromHandle($newHandle);
    }
}
