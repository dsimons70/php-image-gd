<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageColor;
use Goforit\ImageGD\ImageException;

/**
 * Class for several filter functions.
 */
class FilterProcessor implements Processor
{
    private int $type;
    private ?int $arg1;

    /** @var int|bool */
    private mixed $arg2;
    private ?int $arg3;
    private ?int $arg4;

    private function __construct(
        int $filterType,
        int $argument1 = null,
        $argument2 = null,
        int $argument3 = null,
        int $argument4 = null
    ) {
        $this->type = $filterType;
        $this->arg1 = $argument1;
        $this->arg2 = $argument2;
        $this->arg3 = $argument3;
        $this->arg4 = $argument4;
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        $handle = $image->getHandle();
        $type = $this->type;

        switch ($this->countArgs()) {
            case 1:
                $isSuccess = imagefilter($handle, $type, $this->arg1);
                break;
            case 2:
                $isSuccess = imagefilter($handle, $type, $this->arg1, $this->arg2);
                break;
            case 4:
                $isSuccess = imagefilter($handle, $type, $this->arg1, $this->arg2, $this->arg3, $this->arg4);
                break;
            default:
                $isSuccess = imagefilter($handle, $type);
        }

        if (!$isSuccess) {
            throw ImageException::forNotProcessableFilterTypes($this->type);
        }

        return $image;
    }

    /**
     * Named constructor to create a filter to reverse all colors of the image
     */
    public static function forColorReverse(): FilterProcessor
    {
        return new self(IMG_FILTER_NEGATE);
    }

    /**
     * Named constructor to create a filter to colorize the image based on the passed ImageColor
     */
    public static function forColorize(ImageColor $color): FilterProcessor
    {
        return new self(
            IMG_FILTER_COLORIZE,
            $color->getRed(),
            $color->getGreen(),
            $color->getBlue(),
            $color->getAlpha()
        );
    }

    /**
     * Named constructor to create a filter to convert the image into grayscale
     */
    public static function forGrayscale(): FilterProcessor
    {
        return new self(IMG_FILTER_GRAYSCALE);
    }

    /**
     * Named constructor to create a filter to change the brightness of the image
     * The range for the brightness is -255 to +255
     */
    public static function forBrightness(int $brightness): FilterProcessor
    {
        return new self(IMG_FILTER_BRIGHTNESS, $brightness);
    }

    /**
     * Named constructor to create a filter to change the brightness of the image
     * The range for the contrast is: -100 = max contrast, +100 = min contrast (note the direction!)
     */
    public static function forContrast(int $contrast): FilterProcessor
    {
        return new self(IMG_FILTER_CONTRAST, $contrast);
    }

    /**
     * Named constructor to create a filter to highlight the edges in the image
     */
    public static function forEdgeDetection(): FilterProcessor
    {
        return new self(IMG_FILTER_EDGEDETECT);
    }

    /**
     * Named constructor to create a filter to emboss the image
     */
    public static function forEmboss(): FilterProcessor
    {
        return new self(IMG_FILTER_EMBOSS);
    }

    /**
     * Named constructor to create a filter to blur the image using the Gaussian method
     */
    public static function forGaussianBlur(): FilterProcessor
    {
        return new self(IMG_FILTER_GAUSSIAN_BLUR);
    }

    /**
     * Named constructor to create a filter to blurs the image
     */
    public static function forSelectiveBlur(): FilterProcessor
    {
        return new self(IMG_FILTER_SELECTIVE_BLUR);
    }

    /**
     * Named constructor to create a filter to achieve a "sketchy" effect using mean removal
     */
    public static function forMeanRemoval(): FilterProcessor
    {
        return new self(IMG_FILTER_MEAN_REMOVAL);
    }

    /**
     * Named constructor to create a filter to makes the image smoother
     * The range for $level is from -8 to 8
     */
    public static function forSmoothing(int $level = 0): FilterProcessor
    {
        return new self(IMG_FILTER_SMOOTH, $level);
    }

    /**
     * Named constructor to create a filter to pixelate the image
     */
    public static function forPixelate(int $pixelBlockSize, bool $advancedMode = true): FilterProcessor
    {
        return new self(IMG_FILTER_PIXELATE, $pixelBlockSize, $advancedMode);
    }

    /**
     * Return the count of passed arguments
     */
    private function countArgs(): int
    {
        $countArgs = 0;

        if ($this->arg1 !== null) {
            $countArgs++;
        }

        if ($this->arg2 !== null) {
            $countArgs++;
        }

        if ($this->arg3 !== null) {
            $countArgs++;
        }

        if ($this->arg4 !== null) {
            $countArgs++;
        }

        return $countArgs;
    }
}
