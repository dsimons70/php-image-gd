<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;

/**
 * Class to chain image Processor's
 */
class ProcessorChain implements Processor
{
    /** @var Processor[] */
    private array $imageProcessors = [];

    /**
     * Adds a image processor to the chain
     */
    public function addProcessor(Processor $imageProcessor): void
    {
        $this->imageProcessors[] = $imageProcessor;
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        foreach ($this->imageProcessors as $imageProcessor) {
            $image = $imageProcessor->process($image);
        }

        return $image;
    }
}
