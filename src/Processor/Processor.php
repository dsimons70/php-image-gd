<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageException;

/**
 * Interface to process images
 */
interface Processor
{
    /**
     * Processes the passed Image and return the processed Image
     * @throws ImageException
     */
    public function process(Image $image): Image;
}
