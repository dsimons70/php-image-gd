<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Processor;

use Goforit\ImageGD\Image;

/**
 * Class to flip images
 */
class FlipProcessor implements Processor
{
    private int $mode;

    private function __construct(int $mode)
    {
        $this->mode = $mode;
    }

    /**
     * @inheritdoc
     */
    public function process(Image $image): Image
    {
        imageflip($image->getHandle(), $this->mode);

        return $image;
    }

    /**
     * Named constructor to flip the image horizontal
     */
    public static function forHorizontal(): FlipProcessor
    {
        return new self(IMG_FLIP_HORIZONTAL);
    }

    /**
     * Named constructor to flip the image vertical
     */
    public static function forVertical(): FlipProcessor
    {
        return new self(IMG_FLIP_VERTICAL);
    }

    /**
     * Named constructor to flip the image both horizontal and vertical
     */
    public static function forBoth(): FlipProcessor
    {
        return new self(IMG_FLIP_BOTH);
    }
}
