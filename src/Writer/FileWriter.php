<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Writer;

use Exception;
use Goforit\ImageGD\Image;
use Goforit\ImageGD\ImageException;
use SplFileObject;

/**
 * {@inheritdoc}
 *
 * Writes the image to the filesystem. Throws an exception if the filesystem is not writable.
 * All supported php protocols and wrappers like 'ftp://username:password@example.com/image.png'
 * are allowed: @see http://php.net/manual/en/wrappers.php
 */
class FileWriter extends Writer
{
    /**
     * Write the image to the filesystem and returns an SplFileObject on success.
     * We use the extension of the given pathname string to detect the desired image type.
     *
     * @throws ImageException
     */
    public function write(Image $image, string $path): SplFileObject
    {
        try {
            $fileObject = new SplFileObject($path, 'w+');
        } catch (Exception $exception) {
            throw ImageException::forNotWritableFiles($path);
        }

        $this->doWrite($image, $fileObject);

        return $fileObject;
    }

    /**
     * Write the image to the filesystem. We use the file extension of the given pathname to
     * detect the desired image type. Pass an SplFileObject if you want to set stream context
     * options for your pathname. @see http://php.net/manual/en/context.php
     *
     * @throws ImageException
     */
    public function writeFileObject(Image $image, SplFileObject $fileObject): void
    {
        if (!$fileObject->isWritable()) {
            throw ImageException::forNotWritableFiles($fileObject->getPathname());
        }

        $this->doWrite($image, $fileObject);
    }

    /**
     * Writes the image to the filesystem.
     * @throws ImageException
     */
    private function doWrite(Image $image, SplFileObject $fileObject): void
    {
        switch (strtolower($fileObject->getExtension())) {
            case Image::EXTENSION_PNG:
                imagepng($image->getHandle(), $fileObject->getPathname(), $this->pngCompression);
                break;
            case Image::EXTENSION_GIF:
                imagegif($image->getHandle(), $fileObject->getPathname());
                break;
            case Image::EXTENSION_JPG:
            case Image::EXTENSION_JPEG:
                imagejpeg($image->getHandle(), $fileObject->getPathname(), $this->jpgQuality);
                break;
            case Image::EXTENSION_XBM:
                $xbmColor = $image->allocateColor($this->xbmForegroundColor);
                imagexbm($image->getHandle(), $fileObject->getPathname(), $xbmColor);
                break;
            case Image::EXTENSION_WBMP:
                $wbmpColor = $image->allocateColor($this->wbmpForegroundColor);
                imagewbmp($image->getHandle(), $fileObject->getPathname(), $wbmpColor);
                break;
            default:
                throw ImageException::forNotSupportedExtensions($fileObject->getExtension());
        }
    }
}
