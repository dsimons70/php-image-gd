<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Writer;

use Goforit\ImageGD\ImageColor;

/**
 * Abstract writer class to provide several setter functions for image specific write options
 * like jpeg quality or png compression rate.
 */
abstract class Writer
{
    protected int $jpgQuality;
    protected int $pngCompression;
    protected ImageColor $wbmpForegroundColor;
    protected ImageColor $xbmForegroundColor;

    private function __construct()
    {
        $this->wbmpForegroundColor = ImageColor::black();
        $this->xbmForegroundColor = ImageColor::black();
        $this->jpgQuality = 100;
        $this->pngCompression = 0;
    }

    /**
     * Named constructor to create an instance
     */
    public static function create()
    {
        return new static();
    }

    /**
     * Sets the optional foreground color for wbmp images.
     */
    public function setWbmpForegroundColor(ImageColor $wbmpForegroundColor): Writer
    {
        $this->wbmpForegroundColor = $wbmpForegroundColor;

        return $this;
    }

    /**
     * Sets the optional foreground color for xbm images.
     */
    public function setXbmForegroundColor(ImageColor $xbmForegroundColor): Writer
    {
        $this->xbmForegroundColor = $xbmForegroundColor;

        return $this;
    }

    /**
     * Sets the jpg quality from 0 (worst quality) to 100 (best quality).
     */
    public function setJpgQuality(int $quality): Writer
    {
        $isValidQuality = $quality >= 0 && $quality <= 100;
        assert($isValidQuality, 'quality is invalid using default');

        if (!$isValidQuality) {
            $quality = 100;
        }

        $this->jpgQuality = $quality;

        return $this;
    }

    /**
     * Sets the png compression level: from 0 (no compression) to 9.
     */
    public function setPngCompression(int $compression): Writer
    {
        $isValidCompression = $compression >= 0 && $compression <= 9;
        assert($isValidCompression, 'compression is invalid using default');

        if (!$isValidCompression) {
            $compression = 0;
        }

        $this->pngCompression = $compression;

        return $this;
    }
}
