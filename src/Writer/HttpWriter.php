<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Writer;

use Goforit\ImageGD\Image;

/**
 * {@inheritdoc}
 *
 * Writes the image to the output stream to the browser. Sets the valid header
 * content-type based on the selected image extension write function.
 */
class HttpWriter extends Writer
{
    /**
     * Outputs a png image to the browser.
     */
    public function writePng(Image $image)
    {
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_PNG));
        imagepng($image->getHandle(), null, $this->pngCompression);
    }

    /**
     * Output a jpg image to the browser.
     */
    public function writeJpg(Image $image)
    {
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_JPEG));
        imagejpeg($image->getHandle(), null, $this->jpgQuality);
    }

    /**
     * Output a gif image to the browser.
     */
    public function writeGif(Image $image)
    {
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_GIF));
        imagegif($image->getHandle(), null);
    }

    /**
     * Output a xbm image to the browser.
     */
    public function writeXbm(Image $image)
    {
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_XBM));
        imagexbm($image->getHandle(), null, $image->allocateColor($this->xbmForegroundColor));
    }

    /**
     * Output a wbmp image to the browser.
     */
    public function writeWbmp(Image $image)
    {
        header("Content-type: " . image_type_to_mime_type(IMAGETYPE_WBMP));
        imagewbmp($image->getHandle(), null, $image->allocateColor($this->wbmpForegroundColor));
    }
}
