<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD;

use Exception;

/**
 * Class for all library related exceptions
 */
class ImageException extends Exception
{
    /**
     * Named constructor for invalid handles
     */
    public static function forInvalidHandles(): self
    {
        return new self('Handle is invalid');
    }

    /**
     * Named constructor for not supported image extensions
     */
    public static function forNotSupportedExtensions(string $extension): self
    {
        return new self("Image extension not supported: $extension");
    }

    /**
     * Named constructor for non processable or broken image files
     */
    public static function forNotProcessableFiles(string $filePath): self
    {
        return new self("Image file is not processable: $filePath");
    }

    /**
     * Named constructor for not readable image files
     */
    public static function forNotReadableFiles(string $filePath): self
    {
        return new self("File is not readable or does not exists, check path or permission: $filePath");
    }

    /**
     * Named constructor for not writable image files
     */
    public static function forNotWritableFiles(string $filePath): self
    {
        return new self("File is not writable, check path or permission: $filePath");
    }

    /**
     * Named constructor for polygon's with not enough positions
     */
    public static function forPolygonWithoutEnoughPositions(): self
    {
        return new self('At least three positions are required to define an polygon');
    }

    /**
     * Named constructor for not processable filter types
     */
    public static function forNotProcessableFilterTypes(int $type): self
    {
        return new self("Can't apply filter type: $type");
    }
}
