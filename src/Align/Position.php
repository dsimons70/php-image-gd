<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align;

/**
 * Value object to define the x and y coordinate in pixel
 */
class Position
{
    private int $x;
    private int $y;

    /**
     * We use assertions to ensure only positive integer values. Negative values will be
     * converted to positive values
     */
    private function __construct(int $x, int $y)
    {
        assert($x >= 0, 'x must be positive');
        assert($y >= 0, 'y must be positive');

        $this->x = abs($x);
        $this->y = abs($y);
    }

    /**
     * Named constructor to create an instance from the passed int values
     */
    public static function fromInt(int $x, int $y): Position
    {
        return new self($x, $y);
    }

    /**
     * Returns the x coordinate
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * Returns the y coordinate
     */
    public function getY(): int
    {
        return $this->y;
    }
}
