<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Vertical;

use Goforit\ImageGD\Align\Dimension;

/**
 * Interface for all vertical based alignment strategy's
 */
interface VerticalAlign
{
    /**
     * Computes the top left y-position of the passed renderer dimension on the passed image dimension
     */
    public function getTopLeftY(Dimension $imageDimension, Dimension $rendererDimension): int;
}
