<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Vertical;

use Goforit\ImageGD\Align\Alignment;
use Goforit\ImageGD\Align\Dimension;

/**
 * Class to calculate the top left y position for bottom align
 */
class BottomAlign implements VerticalAlign
{
    private Alignment $alignment;

    private function __construct(Alignment $alignment)
    {
        $this->alignment = $alignment;
    }

    /**
     * Named constructor to create an instance from string
     */
    public static function fromString(string $align = '0px'): BottomAlign
    {
        return new self(Alignment::fromString($align));
    }

    /**
     * @inheritdoc
     */
    public function getTopLeftY(Dimension $imageDimension, Dimension $rendererDimension): int
    {
        if ($this->alignment->isPercentage()) {
            $height = round($this->alignment->toPercentage() * $imageDimension->getHeight());
        } else {
            $height = $this->alignment->toAbsolute();
        }

        $y = $imageDimension->getHeight() - $rendererDimension->getHeight() - $height;

        // Prevent negative values
        if ($y < 0) {
            $y = 0;
        }

        return (int)$y;
    }
}
