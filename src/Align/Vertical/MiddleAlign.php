<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Vertical;

use Goforit\ImageGD\Align\Dimension;

/**
 * Class to calculate the top left y position for middle align
 */
class MiddleAlign implements VerticalAlign
{
    /**
     * Named constructor to create an instance
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @inheritdoc
     */
    public function getTopLeftY(Dimension $imageDimension, Dimension $rendererDimension): int
    {
        $imageCenter = $imageDimension->getHeight() / 2;
        $rendererCenter = $rendererDimension->getHeight() / 2;

        $y = round($imageCenter - $rendererCenter);

        // Prevent negative values
        if ($y < 0) {
            $y = 0;
        }

        return (int)$y;
    }
}
