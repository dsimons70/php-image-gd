<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Horizontal;

use Goforit\ImageGD\Align\Dimension;

/**
 * Class to calculate the top left x position for center align
 */
class CenterAlign implements HorizontalAlign
{
    /**
     * Named constructor to create an instance
     */
    public static function create(): CenterAlign
    {
        return new self();
    }

    /**
     * @inheritdoc
     **/
    public function getTopLeftX(Dimension $imageDimension, Dimension $rendererDimension): int
    {
        $imageCenter = round($imageDimension->getWidth() / 2);
        $rendererCenter = round($rendererDimension->getWidth() / 2);

        $x = $imageCenter - $rendererCenter;

        // Prevent negative values
        if ($x < 0) {
            $x = 0;
        }

        return (int)$x;
    }
}
