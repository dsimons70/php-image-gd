<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Horizontal;

use Goforit\ImageGD\Align\Dimension;

/**
 * Interface for all horizontal based alignment strategy's
 */
interface HorizontalAlign
{
    /**
     * Computes the top left x-position of the passed renderer dimension on the passed image dimension
     */
    public function getTopLeftX(Dimension $imageDimension, Dimension $rendererDimension): int;
}

