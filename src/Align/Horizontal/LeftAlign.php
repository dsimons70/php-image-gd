<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align\Horizontal;

use Goforit\ImageGD\Align\Alignment;
use Goforit\ImageGD\Align\Dimension;

/**
 * Class to calculate the top left x position for left align
 */
class LeftAlign implements HorizontalAlign
{
    private Alignment $alignment;

    private function __construct(Alignment $alignment)
    {
        $this->alignment = $alignment;
    }

    /**
     * Named constructor to create an instance from string
     */
    public static function fromString(string $align = '0px'): LeftAlign
    {
        return new self(Alignment::fromString($align));
    }

    /**
     * @inheritdoc
     */
    public function getTopLeftX(Dimension $imageDimension, Dimension $rendererDimension): int
    {
        if ($this->alignment->isPercentage()) {
            return (int)($this->alignment->toPercentage() * $imageDimension->getWidth());
        }

        return $this->alignment->toAbsolute();
    }
}
