<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align;

use Goforit\ImageGD\Align\Horizontal\CenterAlign;
use Goforit\ImageGD\Align\Horizontal\HorizontalAlign;
use Goforit\ImageGD\Align\Horizontal\LeftAlign;
use Goforit\ImageGD\Align\Horizontal\RightAlign;

/**
 * Factory class for all horizontal alignment strategies
 * Acts as a thin layer to provide a more elegant CSS like client interface
 */
class HorizontalAlignment
{
    /**
     * Static factory method to create an instance for left align
     */
    public static function left(string $align): HorizontalAlign
    {
        return LeftAlign::fromString($align);
    }

    /**
     * Static factory method to create an instance for right align
     */
    public static function right(string $align): HorizontalAlign
    {
        return RightAlign::fromString($align);
    }

    /**
     * Static factory method to create an instance for center align
     */
    public static function center(): HorizontalAlign
    {
        return CenterAlign::create();
    }
}
