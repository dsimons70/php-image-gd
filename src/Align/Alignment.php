<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align;

/**
 * Value object to represent percentage and absolute alignment values
 *
 * Supports absolute values in pixel ('20px') and percentage values like ('10%')
 * Negative values like ('-10px') are not allowed
 */
class Alignment
{
    private int $value;
    private string $type;

    /** Allowed types */
    public const ABSOLUTE = 'px';
    public const PERCENTAGE = '%';

    private function __construct(int $value, string $type)
    {
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * Named constructor to create an instance from string
     *
     * Assertions are used to ensure valid positive alignments like '0px', '64px' '5%' and a range
     * from '0%' to '100%' for percentage alignments. We use the default value 0 as an fallback
     * for invalid values
     */
    public static function fromString(string $align): Alignment
    {
        // Ensure valid positive types like '0px', '64px' '5%'
        $isValidAlign = preg_match('/^(\d+)(px|%)$/', $align, $matches) === 1;
        assert($isValidAlign, 'Alignment is invalid using default: 0px');

        if (!$isValidAlign) {
            return new self(0, self::ABSOLUTE);
        }

        $value = (int)$matches[1];
        $type = strtolower($matches[2]);

        if ($type === static::PERCENTAGE) {
            // In percentage mode the value must be in a range from 0 to 100
            $isValidPercentage = $value >= 0 && $value <= 100;
            assert($isValidPercentage, 'Invalid percentage range using default: 0%');

            if (!$isValidPercentage) {
                return new self(0, self::PERCENTAGE);
            }
        }

        return new self($value, $type);
    }

    /**
     * Returns the absolute value.
     *
     * Assertions are used to ensure that this method only gets called for absolute alignments
     * We return 0 if you call this function for an percentage alignment value
     */
    public function toAbsolute(): int
    {
        assert($this->isAbsolute(), 'Trying to access an absolute value in percentage mode');

        if (!$this->isAbsolute()) {
            return 0;
        }

        return $this->value;
    }

    /**
     * Return the percentage value.
     *
     * Converts the percentage value to the proper ratio. Example: 90% will become 0,9
     * Assertions are used to ensure that this method only get called for percentage alignments
     */
    public function toPercentage(): float
    {
        assert($this->isPercentage(), 'Trying to access an percentage value in absolute mode');

        if (!$this->isPercentage()) {
            return 0;
        }

        return $this->value / 100;
    }

    /**
     * Returns the composed string representation like '0px', '50%'
     */
    public function __toString(): string
    {
        return (string)$this->value . $this->type;
    }

    /**
     * Checks for percentage alignment type
     */
    public function isPercentage(): bool
    {
        return $this->type === static::PERCENTAGE;
    }

    /**
     * Checks for absolute alignment type
     */
    public function isAbsolute(): bool
    {
        return $this->type === static::ABSOLUTE;
    }
}
