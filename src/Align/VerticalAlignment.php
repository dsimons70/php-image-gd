<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align;

use Goforit\ImageGD\Align\Vertical\BottomAlign;
use Goforit\ImageGD\Align\Vertical\MiddleAlign;
use Goforit\ImageGD\Align\Vertical\TopAlign;
use Goforit\ImageGD\Align\Vertical\VerticalAlign;

/**
 * Factory class for all vertical alignment strategies
 * Acts as a thin layer to provide a more elegant CSS like client interface
 */
class VerticalAlignment
{
    /**
     *Static factory method to create an instance for top align
     */
    public static function top(string $align): VerticalAlign
    {
        return TopAlign::fromString($align);
    }

    /**
     * Static factory method to create an instance for middle align
     */
    public static function middle(): VerticalAlign
    {
        return MiddleAlign::create();
    }

    /**
     * Static factory method to create an instance for bottom align
     */
    public static function bottom(string $align): VerticalAlign
    {
        return BottomAlign::fromString($align);
    }
}
