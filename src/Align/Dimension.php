<?php

/*
 * This file is part of the Goforit\Image\GD package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Goforit\ImageGD\Align;

/**
 * Value object to store the width and height in pixel
 */
class Dimension
{
    private int $width;
    private int $height;

    /**
     * We use assertions to ensure only positive integer values
     * Negative values are converted to positive values
     */
    private function __construct(int $width, int $height)
    {
        assert($width >= 0, 'Invalid negative value for width');
        assert($height >= 0, 'Invalid negative value for height');

        $this->width = abs($width);
        $this->height = abs($height);
    }

    /**
     * Named constructor to create an instance from the passed int values
     */
    public static function fromInt(int $width, int $height): Dimension
    {
        return new self($width, $height);
    }

    /**
     * Resize percental based on the passed factor
     */
    public function resizePercental(int $factor): Dimension
    {
        $ratio = $factor / 100;

        return Dimension::fromInt(
            (int)round($this->width * $ratio),
            (int)round($this->height * $ratio)
        );
    }

    /**
     * Resize proportional based on the given width
     */
    public function resizeWidth(int $newWidth): Dimension
    {
        $ratio = $newWidth / $this->width;
        $newHeight = (int)round($this->height * $ratio);

        return Dimension::fromInt($newWidth, $newHeight);
    }

    /**
     * Resize proportional based on the given height
     */
    public function resizeHeight(int $newHeight): Dimension
    {
        $ratio = $newHeight / $this->height;
        $newWidth = (int)round($this->width * $ratio);

        return Dimension::fromInt($newWidth, $newHeight);
    }

    /**
     * Resize proportional until it fits into the given dimension
     */
    public function resizeInto(Dimension $newDimension): Dimension
    {
        $oldRatio = $this->width / $this->height;
        $newRatio = $newDimension->getWidth() / $newDimension->getHeight();

        // If the ratio of the new image is lower than the the old image, we have to resize by width
        if ($newRatio <= $oldRatio) {
            return $this->resizeWidth($newDimension->getWidth());
        }

        return $this->resizeHeight($newDimension->getHeight());
    }

    /**
     * Returns the width in px
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Return the height in px
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Return the ratio
     */
    public function getRatio(): float
    {
        return $this->width / $this->height;
    }
}
